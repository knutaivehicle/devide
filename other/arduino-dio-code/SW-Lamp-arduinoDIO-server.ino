//digital INPUT - 2 switches
const byte SW1=2;
const byte SW2=3;

//digital OUTPUT
const byte LEDredRelay=4;
const byte LEDgreenRelay=5;
const byte LEDblueRelay=6;

//serial comm.
char SWstatus[6];         // switch status reporting. buffer
byte LEDcommand[6];       // led control direction. buffer
String inputString = "";        // serialEvent buffer
boolean stringComplete = false; // serialEvent done flag.

void setup()
{
  // initialize serial:
  Serial.begin(9600);

  // reserve 200 bytes for the inputString:
  // for serialEvent.
  inputString.reserve(200);

  //// pin setting - output
  
  // Relay control
  
  // preset:
  //   All *LOW* ==> All LAMP *OFF* at start.
  
  pinMode      (LEDredRelay,   OUTPUT);
  digitalWrite (LEDredRelay,   LOW);
  
  pinMode      (LEDgreenRelay, OUTPUT);
  digitalWrite (LEDgreenRelay, LOW);
  
  pinMode      (LEDblueRelay,  OUTPUT);
  digitalWrite (LEDblueRelay,  LOW);

  //// pin setting - input
  
  // 스위치 (+) == 5V,
  // (+)단과 중간단자는 저항 1k(10k) 연결,
  // 10k와 중간단자가 붙어 있는 것을 pin 2,3으로 각각 하나씩 연결
  // (-) 단자는 GND로 연결
  //
  pinMode(SW1, INPUT); //switch 1, pin 2
  pinMode(SW2, INPUT); //switch 2, pin 3
}

void loop()
{
  if (stringComplete == true)
  {
    inputString.getBytes(LEDcommand, 6); // expecting... ==> {'2', ?, ?, ?, '3', '\n'}

    if (LEDcommand[0] == '2' && LEDcommand[4] == '3')
    {
      // LED can be activated with the status HIGH , ON
      
      if (LEDcommand[1] == '1') digitalWrite (LEDredRelay, HIGH);
      if (LEDcommand[2] == '1') digitalWrite (LEDgreenRelay, HIGH);
      if (LEDcommand[3] == '1') digitalWrite (LEDblueRelay, HIGH);
      
      // LED can be activated with the status HIGH , OFF
      
      if (LEDcommand[1] == '0') digitalWrite (LEDredRelay, LOW);
      if (LEDcommand[2] == '0') digitalWrite (LEDgreenRelay, LOW);
      if (LEDcommand[3] == '0') digitalWrite (LEDblueRelay, LOW);
    }
    
    if ((LEDcommand[1] == 'A') && (LEDcommand[2] == 'A') && (LEDcommand[3] == 'A'))
    {
      SWstatus[0]='2';
      (digitalRead(SW1) == HIGH) ? SWstatus[1] = '1' : SWstatus[1] = '0';
      (digitalRead(SW2) == HIGH) ? SWstatus[2] = '1' : SWstatus[2] = '0';
      SWstatus[3]='3';
      SWstatus[4]='\n';
      SWstatus[5]='\0';
      Serial.print(SWstatus);
      delay(100);
    }

    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

#include <stdint.h>
#include <iostream>
#include <string>

//sample string: $GPGGA,215555.00,3658.19044,N,12752.15095,E,1,06,2.96,105.9,M,20.7,M,,*57
std::string cc = "GPGGA,215555.00,3658.19044,N,12752.15095,E,1,06,2.96,105.9,M,20.7,M,,";

int main()
{
  uint8_t u = 0;
  for (int ii = 0; ii < cc.size(); ii++) {
    u ^= (uint8_t)(cc.c_str()[ii]);
  }

  std::cout << "whole str:" << cc << std::endl;

  std::cout << "chksum:" << u << std::endl; //'W' == 0x57

  return 0;
}

#pragma once

#include <stdio.h>
#include <string.h>

class m8SCANPoint
{
 public:
  float x;
  float y;
  float z;
  float i;
  int   r; //ring

  m8SCANPoint & operator =(const m8SCANPoint & source)
  {
    x = source.x;
    y = source.y;
    z = source.z;
    i = source.i;
    r = source.r; //ring

    return (*this);
  }
  
  // int32_t Log(char * c_str, int32_t length)
  // {
  //   int32_t iStatus;
  //   uint32_t i;
  // 
  //   iStatus = sprintf(c_str, "%.6f,%.6f,%.6f,%.6f,%d\n", \
  //                     x, \
  //                     y, \
  //                     z, \
  //                     i, \
  //                     r);
  // 
  //   return iStatus;
  // }
};

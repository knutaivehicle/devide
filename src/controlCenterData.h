#pragma once

////Control Center Data

#include <stdio.h>
#include <stdint.h>
#include <string.h> //strcmp

// RX - TCP
typedef struct controlIPCAMData_t
{
  int32_t  iObjectID;
  uint64_t uDateTime;
  int16_t  iLength;
  int16_t  iWidth;
  int32_t  iLatitude;
  int32_t  iLongitude;
  int16_t  iLatitude_Vel;
  int16_t  iLongitude_Vel;
  uint16_t uHeading;

} controlIPCAMData;

// TX - TCP
typedef struct controlVehicleData_t
{
  //status byte
  uint8_t  b1_Autonomous_Enable:1;
  uint8_t  b1_Autonomous_Ready:1;
  uint8_t  b1_Autonomous_Control:1;
  uint8_t  b2_Stop_Flag:2;
  uint8_t  b1_Low_Battery:1;
  uint8_t  b1_Reserved1:1;
  uint8_t  b1_Reserved2:1;

  //
  int32_t  iLatitude;
  int32_t  iLongitude;
  uint16_t uHeading;
  uint16_t uSpeed;

} controlVehicleData;

// RX - UDP
typedef struct controlVehicleUDPData_t
{
  //status byte
  uint8_t  uAutonomous_Start;
  uint8_t  uWaypoint;

} controlVehicleUDPData;

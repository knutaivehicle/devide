#pragma once

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h> //toupper

#define I7530A_PARSE_BUFF_SIZE 64

class i7530aCANMessage
{
  ////i7530a protocol
  //parsing state
  static const uint8_t IDLE         = 0x00;
  static const uint8_t GOT_STD_MSG  = 0x01;
  static const uint8_t GOT_STD_ID   = 0x02;
  static const uint8_t GOT_STD_DLC  = 0x03;
  static const uint8_t GOT_STD_DATA = 0x04;
  static const uint8_t GOT_EXT_MSG  = 0x05;
  static const uint8_t GOT_EXT_ID   = 0x06;
  static const uint8_t GOT_EXT_DLC  = 0x07;
  static const uint8_t GOT_EXT_DATA = 0x08;
  //parsing error
  //...

  //
  unsigned int uTemp;
  char printbuf[256];

  //somewhat 'minimal' implementation of hex-to-dec without stdio.h
  //-> https://stackoverflow.com/a/27719325
  //* this functions are NOT really confirmed for many situations. use with EXTREME caution.
  uint8_t hexdigit( char hex ) {
    return (hex <= '9') ? hex - '0' : toupper(hex) - 'A' + 10;
  }
  uint64_t hexn8nibbles( const char* hex, unsigned int n ) { //*up to 4 bytes (8 nibbles)
    //
    uint64_t res = 0;
    //
    res |= hexdigit(*(hex));
    for (unsigned int ii = 1; ii < n; ii++) {
      res = res << 4;
      res |= hexdigit(*(hex + ii));
    }
    return res;
  }
  
public:
  
  double dTimestamp;
  uint32_t uChannel;
  long lID;
  char cMode[2];
  char cResvd;
  uint32_t uDlc;
  uint8_t cData[8];
  char type; // S:STD or E:EXT

  i7530aCANMessage() { }

  int parse_char(char c)
  {
    //static local
    static uint8_t status = i7530aCANMessage::IDLE;
    static char buff[I7530A_PARSE_BUFF_SIZE];
    static uint8_t count = 0;
    static uint8_t data_idx = 0;

    //
    switch (status)
    {
    case i7530aCANMessage::IDLE:
      if ('t' == c) {
        //init msg container
        dTimestamp = 0.0;
        cMode[0] = 'R';
        cMode[1] = 'x';
        type = 'E';
        //reset count && proceed
        count = 0;
        data_idx = 0;
        status = i7530aCANMessage::GOT_STD_MSG;
      } else if ('e' == c) {
        //init msg container
        dTimestamp = 0.0;
        cMode[0] = 'R';
        cMode[1] = 'x';
        type = 'E';
        //reset count && proceed
        count = 0;
        data_idx = 0;
        status = i7530aCANMessage::GOT_EXT_MSG;
      } else {
        //unexpected char. restart
        //reset count && proceed
        count = 0;
        data_idx = 0;
        status = i7530aCANMessage::IDLE;
      }
      break;
    case i7530aCANMessage::GOT_STD_MSG:
      buff[count] = c;
      count++;
      if (count == 3) {
        lID = (long)hexn8nibbles(buff, 3);
        //reset count && proceed
        count = 0;
        data_idx = 0;
        status = i7530aCANMessage::GOT_STD_ID;
      }
      break;
    case i7530aCANMessage::GOT_STD_ID:
      uDlc = c - '0';
      if (uDlc == 0) {
        //reset count && proceed
        count = 0;
        data_idx = 0;
        //**frame received!
        status = i7530aCANMessage::IDLE;
        return 1;
      } else {
        //reset count && proceed
        count = 0;
        data_idx = 0;
        status = i7530aCANMessage::GOT_STD_DLC;
      }
      break;
    case i7530aCANMessage::GOT_STD_DLC:
      buff[count] = c;
      count++;
      if (count == 2) { // 1 byte == 2 char
        cData[data_idx] = (uint8_t)hexn8nibbles(buff, 2);
        data_idx++;
        count = 0;
        if (data_idx == uDlc) {
          //reset count && proceed
          count = 0;
          data_idx = 0;
          //**frame received!
          status = i7530aCANMessage::IDLE;
          return 1;
        }
      }
      break;
    case i7530aCANMessage::GOT_EXT_MSG:
      buff[count] = c;
      count++;
      if (count == 8) {
        lID = (long)hexn8nibbles(buff, 8); //29bits -> 32bits -> 8nibs
        //reset count && proceed
        count = 0;
        data_idx = 0;
        status = i7530aCANMessage::GOT_EXT_ID;
      }
      break;
    case i7530aCANMessage::GOT_EXT_ID:
      uDlc = c - '0';
      if (uDlc == 0) {
        //reset count && proceed
        count = 0;
        data_idx = 0;
        //**frame received!
        status = i7530aCANMessage::IDLE;
        return 1;
      } else {
        //reset count && proceed
        count = 0;
        data_idx = 0;
        status = i7530aCANMessage::GOT_EXT_DLC;
      }
      break;
    case i7530aCANMessage::GOT_EXT_DLC:
      buff[count] = c;
      count++;
      if (count == 2) { // 1 byte == 2 char
        cData[data_idx] = (uint8_t)hexn8nibbles(buff, 2);
        data_idx++;
        count = 0;
        if (data_idx == uDlc) {
          //reset count && proceed
          count = 0;
          data_idx = 0;
          //**frame received!
          status = i7530aCANMessage::IDLE;
          return 1;
        }
      }
      break;
    default:
      ;
    }

    return 0; //**frame is building... i need more.
  }

  //
  void pack_msg_to_send_std_frame(char * c_str, int * len) {
    sprintf(c_str, "t%03x%d", (unsigned int)lID, uDlc); //again prob.? no stdio??
    for (uint32_t ii = 0; ii < uDlc; ii++) {
      // sprintf(printbuf, "%.2x", cData[ii]); // what's .2x ?
      sprintf(printbuf, "%02x", cData[ii]);
      strcat(c_str, printbuf);
    }
    strcat(c_str, "\r"); // cmd should end with '\r'
    (*len) = 5 + uDlc*2 + 1; // "tIIILDD...<CR>" : 5(tIIIL) + 2*uDlc(DD...) + 1 (<CR>
  }

  //
  void pack_msg_to_send_ext_frame(char * c_str, int * len) {
    sprintf(c_str, "e%08x%d", (unsigned int)lID, uDlc); //again prob.? no stdio??
    for (uint32_t ii = 0; ii < uDlc; ii++) {
      // sprintf(printbuf, "%.2x", cData[ii]); // what's .2x ?
      sprintf(printbuf, "%02x", cData[ii]);
      strcat(c_str, printbuf);
    }
    strcat(c_str, "\r"); // cmd should end with '\r'
    (*len) = 10 + uDlc*2 + 1; // "eIIIIIIIILDD...<CR>" : 10(eIIIIIIIIL) + 2*uDlc(DD...) + 1 (<CR>
  }

  int32_t Print(char * c_str, int32_t length)
  {
    int32_t iStatus;
    uint32_t i;

    iStatus = sprintf(c_str, "%.6f %d 0x%03x %cx %c %d ", \
                      dTimestamp, \
                      (uChannel+1), \
                      (unsigned int)lID, \
                      cMode[0], \
                      cResvd, \
                      uDlc);

    for (i = 0; i < uDlc; i++)
    {
      sprintf(printbuf, "%.2x ", cData[i]);
      strcat(c_str, printbuf);
    }

    return iStatus;
  }

  int32_t Log(char * c_str, int32_t length, double dTime)
  {
    int32_t iStatus;
    uint32_t i;

    iStatus = sprintf(c_str, "%.6f,%.6f,%d,%d,%c,%d,%d,", \
                      dTime, \
                      dTimestamp, \
                      uChannel, \
                      (unsigned int)lID, \
                      cMode[0], \
                      cResvd, \
                      uDlc);

    for (i = 0; i < uDlc; i++)
    {
      sprintf(printbuf, "%.2x", cData[i]);
      strcat(c_str, printbuf);
      if (i != uDlc-1) strcat(c_str, ",");
    }
    strcat(c_str, "\n");

    return iStatus;
  }

  i7530aCANMessage & operator =(const i7530aCANMessage & source)
  {
    uint32_t i;

    cMode[0] = source.cMode[0];
    cMode[1] = source.cMode[1];
    cResvd = source.cResvd;
    dTimestamp = source.dTimestamp;
    uChannel = source.uChannel;
    uDlc = source.uDlc;
    lID = source.lID;
    type = source.type;

    for (i = 0; i < source.uDlc; i++)
    {
      cData[i] = source.cData[i];
    }

    return (*this);
  }
};

#include "arduinoSerialInterface.h"

////system
#include <sys/types.h> // open
#include <sys/stat.h> // open
#include <fcntl.h> // open
#include <string.h> // memset
#include <termios.h> // termios
#include <unistd.h> // read
#include <errno.h> // perror
//kernel
#include <linux/serial.h> // serial_struct
#include <sys/ioctl.h> //ioctl, TIOCGSERIAL, TIOCSSERIAL
//http://man7.org/linux/man-pages/man2/ioctl_list.2.html

////framework
#include "ofUtils.h" // ofToHex

//boost log trivial
#include <boost/log/trivial.hpp> // (include only in .cpp)
#define LT_(lvl) BOOST_LOG_TRIVIAL(lvl)

//
#include "hrTimestamp.h"

void arduinoSerialInterface::start()
{
  LT_(info) << "[dio] start thread.";
  b_is_running = true;
  thrd = boost::thread (&arduinoSerialInterface::threadedFunction, this);
}

void arduinoSerialInterface::stop()
{
  LT_(info) << "[dio] stop thread.";
  b_is_running = false;
  //**thread join doesn't work nicely. or just takes too long.
  thrd.join(); 
  LT_(info) << "[dio] thread joined.";
}

void arduinoSerialInterface::logStart(std::string strLogFileName_)
{
  if (bIsLogging == false && b_is_running == true) {
    strLogFileName = strLogFileName_ + s_name + ".csv";
    bIsStartLogReq = true;
    LT_(info) << "[dio] logging start:" << strLogFileName;
  }
}

void arduinoSerialInterface::logStop()
{
  if (bIsLogging == true) {
    bIsStartLogReq = false;
    LT_(info) << "[dio] logging stop.";
  }
}

//config parameters
uint32_t arduinoSerialInterface::config_parameters(std::string s_name_, std::string s_dev_, uint32_t u_baud_)
{
  //device name
  s_name = s_name_;
  s_dev = s_dev_;
  u_baud = u_baud_;

  //shared queue
  rxq.clear();
  txq.clear();
  
  return 0;
}

void arduinoSerialInterface::threadedFunction() {

  //print out configuration info. to the console.
  LT_(info) << "[dio] config: s_dev: " << s_dev;
  LT_(info) << "[dio] config: u_baud: " << u_baud;

  //logging
  FILE * fpLog;
  char logbuff[512];
  hrTimestamp tstamp;

  ////prepare controlling terminal
  //adapted from -> https://github.com/openframeworks/openFrameworks/blob/master/libs/openFrameworks/communication/ofSerial.cpp
  bInited = false;

  //open
  LT_(info) << "[dio] opening " << s_dev << " @ " << u_baud << " bps";
  fd = open(s_dev.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
  if(fd == -1){
    LT_(error) << "[dio] unable to open " << s_dev;
    return;
  }

  //setup - baud
  struct termios options;
  tcgetattr(fd, &oldoptions);
  options = oldoptions;
  switch(u_baud){
  case 300:
    cfsetispeed(&options, B300);
    cfsetospeed(&options, B300);
    break;
  case 1200:
    cfsetispeed(&options, B1200);
    cfsetospeed(&options, B1200);
    break;
  case 2400:
    cfsetispeed(&options, B2400);
    cfsetospeed(&options, B2400);
    break;
  case 4800:
    cfsetispeed(&options, B4800);
    cfsetospeed(&options, B4800);
    break;
  case 9600:
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    break;
  // case 14400:
  //   cfsetispeed(&options, B14400);
  //   cfsetospeed(&options, B14400);
  //   break;
  case 19200:
    cfsetispeed(&options, B19200);
    cfsetospeed(&options, B19200);
    break;
  // case 28800:
  //   cfsetispeed(&options, B28800);
  //   cfsetospeed(&options, B28800);
  //   break;
  case 38400:
    cfsetispeed(&options, B38400);
    cfsetospeed(&options, B38400);
    break;
  case 57600:
    cfsetispeed(&options, B57600);
    cfsetospeed(&options, B57600);
    break;
  case 115200:
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);
    break;
  case 230400:
    cfsetispeed(&options, B230400);
    cfsetospeed(&options, B230400);
    break;
  case 12000000: 
    cfsetispeed(&options, 12000000);
    cfsetospeed(&options, 12000000);	
    break;
  default:
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    LT_(error) << "[dio] cannot set " << u_baud << " bps, setting to 9600";
    break;
  }

  //setup - flags
  options.c_cflag |= (CLOCAL | CREAD);
  options.c_cflag &= ~(PARENB);
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;
  options.c_cflag &= ~CRTSCTS;
  options.c_oflag &= (tcflag_t) ~(OPOST);
  options.c_lflag &= ~(ICANON | ECHO | ISIG);
  options.c_iflag &= (tcflag_t) ~(INLCR | IGNCR | ICRNL | IGNBRK);
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &options); //apply settings

  //using ASYNC_LOW_LATENCY -> https://stackoverflow.com/a/4674755
  struct serial_struct kernel_serial_settings;
  if (ioctl(fd, TIOCGSERIAL, &kernel_serial_settings) == 0) {
    kernel_serial_settings.flags |= ASYNC_LOW_LATENCY;
    ioctl(fd, TIOCSSERIAL, &kernel_serial_settings);
  }

  //opening done.
  bInited = true;
  
  //opening done msg.
  LT_(info) << "[dio] opened " << s_dev << " sucessfully @ " << u_baud << " bps";

  //
  char rbuf[DIO_RX_BUFF_SIZE_MAX];
  int rlen = 0;
  
  //main thread loop
  while (b_is_running == true) {

    //logging - start
    if (bIsLogging == false && bIsStartLogReq == true) {
      bIsStartLogReq = false;
      fpLog = fopen(strLogFileName.c_str(), "w");
      if (fpLog != NULL) {
        bIsLogging = true;
      }
    }

    //Rx
    
    //parsing rs232
    rlen = read(fd, rbuf, DIO_RX_BUFF_SIZE_MAX);
    if (rlen == -1) {
      ;//some error. ignoring...
    }
    else {
      // LT_(info) << "[dio] recv.buf. : " << std::string(rbuf).substr(0, rlen) << ", rlen: " << rlen;
      for (int ii = 0; ii < rlen; ii++)
      {
        if (tMsg.parse_char(rbuf[ii]) == 1) { //frame parsed?
          // //frame received!
          // tMsg.Print(rbuf, DIO_RX_BUFF_SIZE_MAX);
          // LT_(info) << "[dio] parsed : " << rbuf;
          //push to rxQ
          mtx_rxq.lock();
          rxq.push_back(tMsg);
          mtx_rxq.unlock();

          //logging - write one by one
          if (bIsLogging == true) {
            tstamp.get_now();
            tMsg.Log(logbuff, 512, tstamp.d_time);
            fprintf(fpLog, logbuff);
            fflush(fpLog);
          }
        }
      }
    }

    //logging - end
    if (bIsLogging == true && bIsStopLogReq == true) {
      bIsStopLogReq = false;
      fflush(fpLog);
      fclose(fpLog);
      bIsLogging = false;
    }
        
    //Tx
    
    //pack rs232 and spit out
    //(* Rx/Tx duplex doesn't work! let duplex doesn't happen!!)

    int len;
    mtx_txq.lock();
    if (txq.size() > 0) {
      for (unsigned int ii = 0; ii < txq.size(); ii++) {
        // we WON't do serializing here. do it in the main loop.
        // write(fd, txq[ii].c_str(), txq[ii].size());
        write(fd, txq[ii].c_str(), txq[ii].size());
        // LT_(info) << "[dio] sent : " << ofToHex(txq[ii]);
        // fsync(fd);
        tcdrain(fd);
        // tcflush(fd, TCIOFLUSH);
      }
    }
    txq.clear();
    mtx_txq.unlock();

    //
    boost::this_thread::sleep_for(boost::chrono::milliseconds(20)); //20ms
  }

  LT_(info) << "[dio] exiting...";
  
  //close & destory serial
  tcsetattr(fd, TCSANOW, &oldoptions);
  close(fd);

  //
  bInited = false;

  return;
}

//ctor
arduinoSerialInterface::arduinoSerialInterface() {

  //
  b_is_running = false;

  //
  bInited = false;

  //
  bIsLogging = false;
  bIsStartLogReq = false;
  bIsStopLogReq = false;
  strLogFileName = "";
}

//dtor
arduinoSerialInterface::~arduinoSerialInterface() {

  //
  if (bIsLogging == true)
  {
    logStop();
  }

  //
  if (b_is_running == true)
  {
    stop();
  }

  //
  if (bInited == true)
  {
    tcsetattr(fd, TCSANOW, &oldoptions);
    close(fd);
  }
}

//
// **global instance/interface
//

////**instance (initialization)
arduinoSerialInterface __DIO;

////**methods

//
//** RX series return values
//
//   - 0 : no data to get
//   - 0 : yes data to get
//

//RX
int __DIO_RX_ALL (__DIO_message_buffer_t & buf)
{
  __DIO.mtx_rxq.lock();
  if (__DIO.rxq.empty()) {
    __DIO.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    buf = __DIO.rxq; // capture whole buf
    __DIO.rxq.clear();
    __DIO.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//RX
int __DIO_RX_LAST (__DIO_message_t & last)
{
  __DIO.mtx_rxq.lock();
  if (__DIO.rxq.empty()) {
    __DIO.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    last = __DIO.rxq.back(); // capture last
    __DIO.rxq.clear();
    __DIO.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//TX
int __DIO_TX_LEDCTRL (__DIO_message_t msg)
{
  __DIO.mtx_txq.lock();
  __DIO.txq.push_back(msg.create_txmsg_ledControl()); // push to send
  __DIO.mtx_txq.unlock();

  return 1; // always return with success.
}

//TX
int __DIO_TX_SWQUERY (void)
{
  __DIO_message_t msg;
  
  __DIO.mtx_txq.lock();
  __DIO.txq.push_back(msg.create_txmsg_swQuery()); // push to send
  __DIO.mtx_txq.unlock();

  return 1; // always return with success.
}

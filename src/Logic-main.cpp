#include <math.h>
#include "Main.h"
#include "Logic.h"
#include "Logic-main.h"

//boost log trivial
#include <boost/log/trivial.hpp>
#define LT_(lvl) BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::lvl))
#define __LOG    BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::info))

//CAN parsers
//RX
#include "dataCAN191Message.h"
#include "dataCAN192Message.h"
dataCAN191Message tCAN191;
dataCAN192Message tCAN192;

//CAN packers
//TX
#include "dataCAN187Message.h"
#include "dataCAN200to249Message.h"
#include "dataCAN250to269Message.h"
#include "dataCAN270Message.h"
#include "dataCAN271Message.h"
#include "dataCAN272Message.h"

static __DIO_message_t t_switch;
char Autonomous_Enable;
char Autonomous_Ready;
char System_Fail_Flag;
char Battery_Low_Flag;
char Autonomous_go;
char Stop_Flag;
int Autonomous_Control_Cnt;
char Autonomous_Control_Old;
char Autonomous_Control;
char Autonomous_Start;
char Collision_Risk_Flag;
char Autonomous_Control_Trigger_Flag;
char Autonomous_Control_Trigger_Output;
char Autonomous_Control_Trigger_Cnt;
int Vehicle_Busstop_Distance;
int Vehicle_Speed;
char Vehicle_Status;
uint32_t Speed_Tx2Center;
int Status_Tx2Center;
uint32_t Veh_Position_x_Tx2Center;
uint32_t Veh_Position_y_Tx2Center;
int32_t Lat__Tx2Center;
int32_t Long__Tx2Center;
uint16_t Heading__Tx2Center;

double P1_lat;
double P1_long;
double P2_lat;
double P2_long;

gps_distance BusStopValue;

#define PHI  3.141592

char temp_Data;

//scans + meshes
__SICK_scan_t __sick_last;
ofMesh        __sick_mesh;
__M8_scan_t __m8_fl_last;
ofMesh      __m8_fl_mesh;
__M8_scan_t __m8_fr_last;
ofMesh      __m8_fr_mesh;

__GPS_message_t __gps;
void GPS_example(uint32_t loopcnt)
{
  //
  // **GPS
  //

  // (1)
  { //**GPS_RX example
    //
    if (loopcnt % 50 == 0) { // 250 ms
      if (__GPS_RX_LAST(__gps))
      {
        char gpsbuff[256]; //only for formated printing..
        __gps.Print(gpsbuff, 256);
        __LOG << "[ex:gps] : gps : " << std::string(gpsbuff);
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }
}

void CONTROL_example(uint32_t loopcnt)
{
  //CCENTER - Vehicle TX. example/test
  if (loopcnt % 3 == 0) { // 60ms
    __CCENTER_VEHICLE_message_t txmsg;
    txmsg.b1_Autonomous_Enable = 1;
    // txmsg.b1_Autonomous_Ready = 1;
    txmsg.b1_Autonomous_Ready = 0;
    txmsg.b1_Autonomous_Control = 0;
    // txmsg.b2_Stop_Flag = 3;
    txmsg.b2_Stop_Flag = 0;
    txmsg.b1_Low_Battery = 0;
    txmsg.iLatitude = (int32_t)(__gps.dLat*100000); //URGENT fix: wrong data.. later fix.
    txmsg.iLongitude = (int32_t)(__gps.dLongt*100000); //URGENT fix: wrong data
    txmsg.uHeading = 9012;
    txmsg.uSpeed = 3456;
    __CCENTER_TX_VEHICLE_DATA (txmsg);
    // __LOG << "__CCENTER_VEHICLE_message_t: size:" << sizeof(__CCENTER_VEHICLE_message_t);
  }

  //CCENTER - IPCAM RX. example/test
  if (loopcnt % 2 == 0) { // 40ms
    __CCENTER_IPCAM_message_buffer_t rxq;
    if (__CCENTER_RX_IPCAM_BUF(rxq))
    {
      for (uint32_t i = 0; i < rxq.size(); i++) {
        __LOG << "[ccenter/listen] ipcam data: objectid:" << __CCENTER.rxq[i].iObjectID
              << " datetime:" << __CCENTER.rxq[i].uDateTime
              << " length:" << __CCENTER.rxq[i].iLength
              << " width:" << __CCENTER.rxq[i].iWidth
              << " longitude:" << __CCENTER.rxq[i].iLongitude
              << " latitude:" << __CCENTER.rxq[i].iLatitude
              << " long_vel:" << __CCENTER.rxq[i].iLongitude_Vel
              << " lat_vel:" << __CCENTER.rxq[i].iLatitude_Vel
              << " heading:" << __CCENTER.rxq[i].uHeading;
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }

  //CCENTER - Vehicle RX. (UDP) example/test
  if (loopcnt % 2 == 0) { // 40ms
    __CCENTER_VEHICLE_UDP_message_buffer_t rxq;
    if (__CCENTER_RX_VEHICLE_UDP_BUF(rxq))
    {
      for (uint32_t i = 0; i < rxq.size(); i++) {
        __LOG << "[ccenter/udp] vehicle cmd.:" << rxq[i].uAutonomous_Start
              << " waypoint:" << rxq[i].uWaypoint;
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }
}

void DIO_CAN_Comm(uint32_t loopcnt)
{
  // //DIO SW status query
  // if (loopcnt % 80 == 0) { // 0.8s
  //   __DIO_TX_SWQUERY ();

  //   __MSLEEP(30); // DIO service loop time == 30ms

  //   if (__DIO_RX_LAST(t_switch))
  //   {
  //     __LOG << "[ex:dio] #2 sw stat: (sw1:"
  //           << last.uSW1
  //           << "),(sw2:"
  //           << last.uSW2
  //           << ")";
  //   }
  //   else
  //   {
  //     ;// Come back next time! (No data available)
  //   }
  // }

  //DIO SW status query
  if (loopcnt % 5 == 0) { // 100ms
    __DIO_TX_SWQUERY ();
    __MSLEEP(30);
    __DIO_RX_LAST(t_switch);
  }

  //0x187 packing test.
  if (loopcnt % 50 == 0) { // 1 sec.
    dataCAN187Message PC2Vehmsg;
    //
    PC2Vehmsg.uPCcon_En = 1;
    PC2Vehmsg.uPCcon_ST = 0;
    PC2Vehmsg.uPCcon_Speed = 7;
    //
    PC2Vehmsg.packTxCANMsg();

    char testbuf[512];
    PC2Vehmsg.Print(testbuf, 512);
    __LOG << testbuf;
  }

  //0x250 ~ 0x269 packing test.
  if (loopcnt % 50 == 0) { // 1 sec.
    dataCAN250to260Message PC2Vehmsg_32bits;
    //
    PC2Vehmsg_32bits.uInt32_0 = 0b11100101100110000101100100110111;
    //0b 1110 0101 1001 1000 0101 1001 0011 0111;
    //cData: 3-0xe5 2-0x98 1-0x59 0-0x37

    PC2Vehmsg_32bits.uInt32_1 = 0b01100101010110101011001001101110;
    //0b 0110 0101 0101 1010 1011 0010 0110 1110;
    //cData: 7-0x65 6-0x5a 5-0xb2 4-0x6e

    //
    PC2Vehmsg_32bits.packTxCANMsg();

    char testbuf[512];
    PC2Vehmsg_32bits.Print(testbuf, 512);
    __LOG << testbuf;
  }



  /*
    if(t_switch.uSW1==1)//PC2Vehmsg 0 0 0 0 0 0 0 1
    {
    ;
    }
    else
    {
    ;
    }

    if(t_switch.uSW2==1)//PC2Vehmsg 0 0 0 0 0 0 1 0    // //PC2Vehmsg 0 0 0 0 0 0 1 1
    {
    ;
    }
  */

  // CAN CH 1
  if (loopcnt % 3 == 0) { // 60ms
    __CAN_message_buffer_t rxq;
    if (__CANCH1_RX_BUF(rxq)) // ch1 RX
    {
      char canch1buff[256]; //only for formated printing..
      for (uint32_t i = 0; i < rxq.size(); i++) {
        if (rxq[i].lID != 0x1fffffec && rxq[i].lID != 0x1ffffffc)
        {
          rxq[i].Print(canch1buff, 256);
          // __LOG << "[ex:can] : recv. (ch1) : " << std::string(canch1buff);

          //
          tCAN191.parseRxCANMessage(&(rxq[i]));
          char can191buff[500]; //only for formated printing..
          tCAN191.Print(can191buff, 500);
          __LOG << "[ex:can] : 0x191 parsed : " << std::string(can191buff);

          //
          tCAN192.parseRxCANMessage(&(rxq[i]));
          char can192buff[500]; //only for formated printing..
          tCAN192.Print(can192buff, 500);
          __LOG << "[ex:can] : 0x192 parsed : " << std::string(can192buff);
        }
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }

  // //CAN1 TX
  // if (loopcnt % 3 == 0) { // 3 --> 1--->3     60ms

  //   PC2Vehmsg.type = 'E';         // EXT frame
  //   PC2Vehmsg.lID = 0x187;        // id 0x100
  //   PC2Vehmsg.uDlc = 1;           // dlc : 1
  //   // PC2Vehmsg.cData[0] = loopcnt; // data bytes : [0] XX
  //   //PC2Vehmsg.cData[0]=t_switch.uSW1 | (Autonomous_Control_Trigger_Output<<1);
  //   PC2Vehmsg.cData[0]=t_switch.uSW1 | (Autonomous_Control_Trigger_Output<<1)|(temp_Data<<2);
  //   //
  //   __CANCH1_TX (PC2Vehmsg);      //STD & EXT send
  // }
}



void Signal_Processing(uint32_t loopcnt)
{
  //Vehicle_Busstop_Distance=BusStopPositionCheck(Vehcile_Lat,Vehicle_Lon,BusStop_Lat,BusStop_Lon);
  System_Fail_Flag=0;
  Battery_Low_Flag=0;
  Autonomous_Enable=t_switch.uSW1;
  Autonomous_go=t_switch.uSW2 ;

  Autonomous_Start=0;

  DIO_CAN_Comm(loopcnt);

  P1_lat=0;
  P1_long=0;
  P2_lat=0;
  P2_long=0;
  BusStopValue = gps_2_point(P1_lat, P1_long, P2_lat, P2_long);

  if (__LOGIC.fpLogic != NULL) {
    //CSV (comma separated values) --> matlab csv!
    fprintf(__LOGIC.fpLogic, "10,200,100,500\n");
    fprintf(__LOGIC.fpLogic, "%d,%.6f,7890\n", P1_lat, P1_long);
    fflush(__LOGIC.fpLogic);
  }
}

void Autonomous_Start_Check(uint32_t loopcnt)
{

  //=BusStopPositionCheck(Vehcile_Lat,Vehicle_Lon,BusStop_Lat,BusStop_Lon);
  if(Autonomous_Enable==1)
  {
    if(Autonomous_Ready==0)
    {
      if(System_Fail_Flag==0 && Battery_Low_Flag==0 && Autonomous_go==1)
      {
        Autonomous_Ready=1;
        Stop_Flag=1;
      }

    }
    else //Autonomous_Ready==1
    {
      if(System_Fail_Flag==1 || Battery_Low_Flag== 1 || (Autonomous_Control==0 && Autonomous_Control_Old==1) )//|| Autonomous_go==0)
      {
        Autonomous_Ready=0;
        Autonomous_Control_Cnt=0;
      }
      else
      {
        // Test : set
        Autonomous_Start =1;
        Autonomous_Control_Old= Autonomous_Control;
        if(Autonomous_Control==0) //
        {
          if((Autonomous_Start==1 && Stop_Flag==1 && Autonomous_go==1)  || (Collision_Risk_Flag==0 && Stop_Flag==2 && Autonomous_go==1))
          {
            Autonomous_Control_Cnt++;
            if(Autonomous_Control_Cnt >=150)
            {
              Autonomous_Control=1;
              Stop_Flag=0;
            }
            else
            {
              ;
            }
          }
          else
          {
            Autonomous_Control_Cnt=0;
          }
        }
        else // Autonomous_Control==1
        {
          Autonomous_Control_Cnt=0;
          if(Autonomous_Start==0 || Autonomous_go==0)
          {
            Autonomous_Control=0;
          }
          else
          {
            if(Stop_Flag==2)
            {
              Autonomous_Control=0;
            }
            else if(Stop_Flag==1)
            {
              Autonomous_Control=0;
            }
            else if(Autonomous_go==0)
            {
              Autonomous_Control=0;
            }
            else
            {
              ;
            }
          }
        }
      }
    }
  }
  else
  {
    ;
  }
}



void Autonomous_Control_Action(uint32_t loopcnt)
{
  if(Autonomous_Control_Trigger_Flag==0)
  {

    if((Autonomous_Control_Old==0 && Autonomous_Control==1) || (Autonomous_Control_Old==1 && Autonomous_Control==0))

    {
      Autonomous_Control_Trigger_Flag=1;
      Autonomous_Control_Trigger_Output=1;
      Autonomous_Control_Trigger_Cnt=0;
    }
    else
    {
      ;
    }
  }
  else //if(Autonomous_Control_Trigger_Flag==1)
  {
    Autonomous_Control_Trigger_Cnt++;
    if(Autonomous_Control_Trigger_Cnt <=10)
    {
      Autonomous_Control_Trigger_Output=1;
      Autonomous_Control_Trigger_Flag=1;
      if(Autonomous_Control_Old==1)
      {
        Autonomous_Control_Old=0;
      }
    }
    else
    {
      Autonomous_Control_Trigger_Flag=0;
      Autonomous_Control_Trigger_Output=0;
    }
  }
}


void Lidar_Object_Tracking(uint32_t loopcnt)
{

}
void Path_Generation(uint32_t loopcnt)
{

}
void Infra_Information_Processing(uint32_t loopcnt)
{


}
void Collision_Estimation(uint32_t loopcnt)
{


}
void Stop_Check(uint32_t loopcnt)
{
  //Vehicle_Busstop_Distance=BusStopPositionCheck(Vehcile_Lat,Vehicle_Lon,BusStop_Lat,BusStop_Lon);
  Vehicle_Busstop_Distance=1;
  if(Stop_Flag==0)
  {
    if(Collision_Risk_Flag==1)
    {
      Stop_Flag=2;
    }
    else if(Vehicle_Speed== 0 && Vehicle_Busstop_Distance <=1) //Autonomous_start=1ision_Risk_Flag==0
    {
      Stop_Flag=1;
    }
    else
    {
      ;
    }


  }
  else if(Stop_Flag==1)
  {
    if(Autonomous_Control==1)
    {
      Stop_Flag=0;
    }
    else
    {
    }
  }
  else  // Stop_Flag==2
  {
    if(Autonomous_Control==1)
    {
      Stop_Flag=0;
    }
    else
    {
    }
  }
}


void Output_Signal_Processing(uint32_t loopcnt)
{
  //Veh_Position_x_Tx2Center;
  //uint32_t Veh_Position_y_Tx2Center;
  Vehicle_Status=(Autonomous_Enable )| (Autonomous_Ready)|(Autonomous_Control)|(Stop_Flag)|(Battery_Low_Flag);

  Status_Tx2Center=Vehicle_Status;
  Lat__Tx2Center=0;
  Long__Tx2Center=0;
  Heading__Tx2Center=0;
  Speed_Tx2Center=Vehicle_Speed;


}







gps_distance   gps_2_point(double P1_lat, double P1_long, double P2_lat, double P2_long)
{


  double e10;
  double e11;
  double e12;
  double e13;

  double c16;
  double c15;
  double c17;

  double f15;
  double f16;
  double f17;
  double f18;
  double f19;

  double c18;
  double c20;
  double c21;
  double c22;
  double c23;
  double c24;
  double c25;
  double c26;
  double c27;

  double c29;
  double c31;
  double c33;
  double c35;
  double c36;
  double c38=0;
  double c40=0;
  double c41;
  double c43;
  double c45;
  double c47;
  double c50;
  double c52;
  double c54;

  double Cur_Lat_radian;
  double Cur_Lon_radian;
  double Dest_Lat_radian;
  double Dest_Lon_radian;
  double radian_distance;
  double radian_bearing;
  double true_bearing;


  gps_distance  value;


  if ((P1_lat == P2_lat) && (P1_long==P2_long))
  {

    value. Dist =0;
    value. Bearing_Angle=0;
  }


  e10 = P1_lat*PHI / 180;
  e11 = P1_long*PHI / 180;
  e12 = P2_lat*PHI / 180;
  e13 = P2_long*PHI / 180;

  c16 = 6356752.314140910;
  c15 = 6378137.0000000000;
  c17 = 0.0033528107;
  f15 = c17 + c17*c17;
  f16 = f15 / 2;
  f17 = c17*c17 / 2;
  f18 = c17*c17 / 8;
  f19 = c17*c17 / 16;
  c18 = e13 - e11;
  c20 = (1 - c17)*tan(e10);
  c21 = atan(c20);
  c22 = sin(c21);
  c23 = cos(c21);
  c24 = (1 - c17)*tan(e12);
  c25 = atan(c24);
  c26 = sin(c25);
  c27 = cos(c25);
  c29 = c18;
  c31 = (c27*sin(c29)*c27*sin(c29) + (c23*c26 - c22*c27*cos(c29))*(c23*c26 - c22*c27*cos(c29)));
  c33 = (c22*c26) + (c23*c27*cos(c29));
  c35 = sqrt(c31) / c33;
  c36 = atan(c35);

  if (c31 <= 0) {
    c38 = 0;
  }
  else {
    c38 = c23*c27*sin(c29) / sqrt(c31);
  }

  if ((cos(asin(c38))*cos(asin(c38))) == 0) {
    c40 = 0;
  } else {
    c40 = c33 - 2 * c22*c26 / (cos(asin(c38))*cos(asin(c38)));
  }

  c41 = cos(asin(c38))*cos(asin(c38))*(c15*c15 - c16*c16) / (c16*c16);
  c43 = 1 + c41 / 16384 * (4096 + c41 * (-768 + c41 *(320 - 175 * c41)));
  c45 = c41 / 1024 * (256 + c41 *(-128 + c41*(74 - 47 * c41)));
  c47 = c45 * sqrt(c31) * (c40 + c45 / 4 * (c33*(-1 + 2 * c40*c40) - c45 / 6 * c40*(-3 + 4 * c31)*(-3 + 4 * c40*c40)));
  c54 = c16*c43*(atan(c35) - c47);

  value. Dist = c54;


  Cur_Lat_radian = P2_lat*(3.141592 / 180);
  Cur_Lon_radian = P2_long*(3.141592 / 180);

  Dest_Lat_radian = P2_lat*(3.141592 / 180);
  Dest_Lon_radian = P2_long*(3.141592 / 180);

  radian_distance = acos(sin(Cur_Lat_radian)*sin(Dest_Lat_radian) + cos(Cur_Lat_radian)*cos(Dest_Lat_radian)*cos(Cur_Lon_radian - Dest_Lon_radian));

  radian_bearing = acos((sin(Dest_Lat_radian) - sin(Cur_Lat_radian)*cos(radian_distance)) / (cos(Cur_Lat_radian)*sin(radian_distance)));


  if (sin(Dest_Lon_radian - Cur_Lon_radian)< 0 ) {
    true_bearing = radian_bearing*(180/PHI);
    true_bearing = 360 - true_bearing;}
  else{
    true_bearing = radian_bearing*(180/PHI);
  }

  value. Bearing_Angle = true_bearing;

  return value;

}

// graphics functions only works in draw() functions!!
// we want to draw this data so made it global.
void SICK_view(uint32_t loopcnt)
{
  //
  // **SICK
  //

  // (1)
  { //**SICK_RX example
    //
    if (loopcnt % 5 == 0) { // 100ms

      if (__SICK_RX_LAST(__sick_last)) {
        //CLEAR
        __sick_mesh.clear();

        //RELOAD
        for (unsigned int pnt = 0; pnt < SICK_COV_PNTS_MAX; pnt++) //iterate over 'points' in the scan
        {
          //conversion!!!

          __sick_mesh.addVertex(ofVec3f(__sick_last.x[pnt],
                                        __sick_last.y[pnt],
                                        __sick_last.z[pnt]));
          __sick_mesh.addColor(ofFloatColor(0,0,255)); //blue
        }
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }
}


//for M8 Logging!!
char logbuff_pnt[200];
int32_t actual_pnts = 0;
hrTimestamp __m8_hrtimer;

////
//M8 Front Left logging
//
FILE * __m8_fl_fp_log = NULL;
bool   __m8_fl_b_is_logging = false;

void M8_FL_logstart(std::string logpath)
{
  if (__m8_fl_b_is_logging == false)
  {
    std::string fullname = logpath + "M8_FL.csv";
    __m8_fl_fp_log = fopen(fullname.c_str(), "w");
    if (__m8_fl_fp_log != NULL) {
      __m8_fl_b_is_logging = true;
    }
  }
}

void M8_FL_logstop()
{
  if (__m8_fl_b_is_logging == true)
  {
    fflush(__m8_fl_fp_log);
    fclose(__m8_fl_fp_log);
  }
}

//M8 FL viewer + **logger
void M8_FL_view(uint32_t loopcnt)
{
  // (1)
  { //**M8_RX example
    //
    if (loopcnt % 4 == 0) { // 80ms

      if (__M8_FL_RX_LAST(__m8_fl_last)) {
        // __LOG << "[ex:m8] : m8 #of points : " << __m8_fl_last.size();

        //mesh update

        //CLEAR
        __m8_fl_mesh.clear();

        //logging
        if (__m8_fl_b_is_logging == true) {
          //
          __m8_hrtimer.get_now();
          // sprintf(logbuff_pnt, "%.6f,%d,%d\n", __m8_hrtimer.d_time, __m8_fl_last.size()); // scan tstamp, #pnts
          sprintf(logbuff_pnt, "%.6f,%d\n", __m8_hrtimer.d_time); // scan tstamp, #pnts
          fprintf(__m8_fl_fp_log, logbuff_pnt);
          fflush(__m8_fl_fp_log);
        }

        //RELOAD
        actual_pnts = 0;
        for (unsigned int pnt = 0; pnt < __m8_fl_last.size(); pnt++) //iterate over 'points' in the scan
        {
          //__m8_fl_last[pnt].x/y/z/i/r .... -> convertion!!!!!

          // uncomment this to see raw data but.. be prepared. too many data.

          //logging
          if (__m8_fl_b_is_logging == true) {
            if (isnan(__m8_fl_last[pnt].x)) {
              sprintf(logbuff_pnt, "%.6f,%.6f,%.6f,%.6f,%d\n", \
                      __m8_fl_last[pnt].x, \
                      __m8_fl_last[pnt].y, \
                      __m8_fl_last[pnt].z, \
                      __m8_fl_last[pnt].i, \
                      __m8_fl_last[pnt].r);

              fprintf(__m8_fl_fp_log, logbuff_pnt);
              fflush(__m8_fl_fp_log);
              actual_pnts++;
            }
          }

          // //console printing of m8 data
          // __LOG << "[ex:m8] : m8 data-x:" << __m8_fl_last[pnt].x
          //       << ",y:" << __m8_fl_last[pnt].y
          //       << ",z:" << __m8_fl_last[pnt].z
          //       << ",i:" << __m8_fl_last[pnt].i
          //       << ",r:" << __m8_fl_last[pnt].r;

          //graphics for m8 scan - preparation (loading)

          //load m8 poing cloud
          __m8_fl_mesh.addVertex(ofVec3f(__m8_fl_last[pnt].x, __m8_fl_last[pnt].y, __m8_fl_last[pnt].z));

          // ring coloring
          switch (__m8_fl_last[pnt].r)
          {
          case 0:
            __m8_fl_mesh.addColor(ofFloatColor(255,0,0)); //red
            break;
          case 1:
            __m8_fl_mesh.addColor(ofFloatColor(0,255,0)); //green
            break;
          case 2:
            __m8_fl_mesh.addColor(ofFloatColor(0,0,255)); //blue
            break;
          case 3:
            __m8_fl_mesh.addColor(ofFloatColor(255,255,0)); //yellow
            break;
          case 4:
            __m8_fl_mesh.addColor(ofFloatColor(0,255,255)); //cyan
            break;
          case 5:
            __m8_fl_mesh.addColor(ofFloatColor(255,0,255)); //magenta
            break;
          case 6:
            __m8_fl_mesh.addColor(ofFloatColor(255,255,255)); //white
            break;
          case 7:
            __m8_fl_mesh.addColor(ofFloatColor(0,0,0)); //black
            break;
          default:
            ;
          }
        }
        //
        sprintf(logbuff_pnt, "%d\n", actual_pnts); // scan tstamp, #pnts
        fprintf(__m8_fl_fp_log, logbuff_pnt);
        fflush(__m8_fl_fp_log);
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }
}

////
//M8 Front Rear logging
//
FILE * __m8_fr_fp_log = NULL;
bool   __m8_fr_b_is_logging = false;

void M8_FR_logstart(std::string logpath)
{
  if (__m8_fr_b_is_logging == false)
  {
    std::string fullname = logpath + "M8_FR.csv";
    __m8_fr_fp_log = fopen(fullname.c_str(), "w");
    if (__m8_fr_fp_log != NULL) {
      __m8_fr_b_is_logging = true;
    }
  }
}

void M8_FR_logstop()
{
  if (__m8_fr_b_is_logging == true)
  {
    fflush(__m8_fr_fp_log);
    fclose(__m8_fr_fp_log);
  }
}

//M8 FR viewer + **logger
void M8_FR_view(uint32_t loopcnt)
{
  // (1)
  { //**M8_RX example
    //
    if (loopcnt % 4 == 0) { // 80ms

      if (__M8_FR_RX_LAST(__m8_fr_last)) {
        // __LOG << "[ex:m8] : m8 #of points : " << __m8_fr_last.size();

        //mesh update

        //CLEAR
        __m8_fr_mesh.clear();

        //logging
        if (__m8_fr_b_is_logging == true) {
          //
          __m8_hrtimer.get_now();
          sprintf(logbuff_pnt, "%.6f,%d,%d\n", __m8_hrtimer.d_time, __m8_fr_last.size()); // scan tstamp, #pnts
          fprintf(__m8_fr_fp_log, logbuff_pnt);
          fflush(__m8_fr_fp_log);
        }

        //RELOAD
        for (unsigned int pnt = 0; pnt < __m8_fr_last.size(); pnt++) //iterate over 'points' in the scan
        {
          // uncomment this to see raw data but.. be prepared. too many data.

          //logging
          if (__m8_fr_b_is_logging == true) {
            //__m8_fr_last[pnt].Log(logbuff_pnt, 200); <-- DOES NOT work. but why? no idea.
            sprintf(logbuff_pnt, "%.6f,%.6f,%.6f,%.6f,%d\n", \
                    __m8_fr_last[pnt].x,          \
                    __m8_fr_last[pnt].y, \
                    __m8_fr_last[pnt].z, \
                    __m8_fr_last[pnt].i, \
                    __m8_fr_last[pnt].r);

            fprintf(__m8_fr_fp_log, logbuff_pnt);
            fflush(__m8_fr_fp_log);
          }

          // //console printing of m8 data
          // __LOG << "[ex:m8] : m8 data-x:" << __m8_fr_last[pnt].x
          //       << ",y:" << __m8_fr_last[pnt].y
          //       << ",z:" << __m8_fr_last[pnt].z
          //       << ",i:" << __m8_fr_last[pnt].i
          //       << ",r:" << __m8_fr_last[pnt].r;

          //graphics for m8 scan - preparation (loading)

          //load m8 poing cloud
          __m8_fr_mesh.addVertex(ofVec3f(__m8_fr_last[pnt].x, __m8_fr_last[pnt].y, __m8_fr_last[pnt].z));

          // ring coloring
          switch (__m8_fr_last[pnt].r)
          {
          case 0:
            __m8_fr_mesh.addColor(ofFloatColor(255,0,0)); //red
            break;
          case 1:
            __m8_fr_mesh.addColor(ofFloatColor(0,255,0)); //green
            break;
          case 2:
            __m8_fr_mesh.addColor(ofFloatColor(0,0,255)); //blue
            break;
          case 3:
            __m8_fr_mesh.addColor(ofFloatColor(255,255,0)); //yellow
            break;
          case 4:
            __m8_fr_mesh.addColor(ofFloatColor(0,255,255)); //cyan
            break;
          case 5:
            __m8_fr_mesh.addColor(ofFloatColor(255,0,255)); //magenta
            break;
          case 6:
            __m8_fr_mesh.addColor(ofFloatColor(255,255,255)); //white
            break;
          case 7:
            __m8_fr_mesh.addColor(ofFloatColor(0,0,0)); //black
            break;
          default:
            ;
          }
        }
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }
}

//**
// *********!!! REMEMBER call each mesh's draw() in Logic's draw() !!!
//

// all of* is from openFrameworks graphics engine.

// link: reference : http://openframeworks.cc/documentation/
// link: reading   : http://openframeworks.cc/ofBook/chapters/foreword.html

void graphics_setup()
{
  __sick_mesh.setMode(OF_PRIMITIVE_POINTS);
  __m8_fl_mesh.setMode(OF_PRIMITIVE_POINTS);
  __m8_fr_mesh.setMode(OF_PRIMITIVE_POINTS);

}

void graphics_draw()
{
  ofBackground(0);

  //sensor meshes
  __sick_mesh.draw();
  __m8_fl_mesh.draw();
  __m8_fr_mesh.draw();

  //ofDrawGrid(1, 5, true);

  ofPushMatrix();
  ofSetColor(100,100,100); // gray
  ofRotateY(90);
  ofDrawGridPlane(1, 5, true); // draw YZ plane -> rotateY -> XY plane
  ofPopMatrix();

  ofSetColor(255,0,0); // red
  ofDrawLine(-5,0,0.01,5,0,0.01); // x-axis mark

  // draw a line
  ofSetColor(0,255,0,255);
  ofPolyline line;
  for (int i = 0; i < 100; i++){
    float a = 1;
    float b = 1;
    float c = 1;
    float d = 1;
    //
    float px = i*0.1;
    float py = a*px*px*px + b*px*px + c*px + d;
    line.addVertex(ofVec2f(px,py));
  }
  // draw the line
  line.draw();

  //drawing a box
  ofPushMatrix();
  ofVec3f pos(0.2,0.3,0);
  float boxSize = 0.03;
  ofTranslate(pos);
  ofRotateZ(pos.z); //radian
  ofFill(); //
  ofSetColor(255); // white: 255, 255, 255
  // ofDrawBox(boxSize);
  ofDrawBox(boxSize);
  //
  ofVec3f pos2(1.2,-0.3,0);
  ofTranslate(pos2);
  ofRotateZ(pos2.z); //radian
  ofFill(); //
  ofSetColor(255, 200, 60);
  // ofDrawBox(boxSize);
  ofDrawSphere(0.5);
  ofPopMatrix();
}

ofTrueTypeFont font;
void graphics_overlay()
{
  ofTrueTypeFont::setGlobalDpi(72);
  font.load("DejavuSansMono.ttf", 14, true, true);
  font.setLineHeight(18.0f);
  font.setLetterSpacing(1.037);
  font.drawString("HELLO:0.56234 seconds", 10, 30);
}

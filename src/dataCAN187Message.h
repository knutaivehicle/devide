#pragma once

#include <stdint.h>
#include "i7530aCANMessage.h"

struct dataCAN187Message : public i7530aCANMessage
{
  uint8_t uByte7;
  uint8_t uByte6;
  uint8_t uByte5;
  uint8_t uByte4;
  uint8_t uByte3;
  uint8_t uByte2;
  //Byte 1
  union {
    struct {
      uint8_t uPCcon_En:1;
      uint8_t uPCcon_ST:1;
      uint8_t uPCcon_Speed:3;
      uint8_t uSP_0:1;
      uint8_t uSP_1:1;
      uint8_t uSP_2:1;
    };
    uint8_t uByte1;
  };
  //Byte 0
  union {
    struct {
      uint8_t uSP_3:1;
      uint8_t uSP_4:1;
      uint8_t uSP_5:1;
      uint8_t uSP_6:1;
      uint8_t uSP_7:1;
      uint8_t uSP_8:1;
      uint8_t uSP_9:1;
      uint8_t uSP_10:1;
    };
    uint8_t uByte0;
  };

  dataCAN187Message()
  {
    //undefined
    dTimestamp = 0;
    uChannel = 0;
    //defined
    lID = 0x187;
    cMode[0] = 'T';
    cMode[1] = 'x';
    cResvd = 'd';
    uDlc = 8;
    type = 'E';
    //default
    uPCcon_En = 0;
    uPCcon_ST = 0;
    uPCcon_Speed = 0;
    uSP_0 = 0;
    uSP_1 = 0;
    uSP_2 = 0;
    uSP_3 = 0;
    uSP_4 = 0;
    uSP_5 = 0;
    uSP_6 = 0;
    uSP_7 = 0;
    uSP_8 = 0;
    uSP_9 = 0;
    uSP_10 = 0;
    //default
    uByte0 = 0;
    uByte1 = 0;
    uByte2 = 0;
    uByte3 = 0;
    uByte4 = 0;
    uByte5 = 0;
    uByte6 = 0;
    uByte7 = 0;
  }

  void packTxCANMsg()
  {
    cData[0] = uByte0;
    cData[1] = uByte1;
    cData[2] = uByte2;
    cData[3] = uByte3;
    cData[4] = uByte4;
    cData[5] = uByte5;
    cData[6] = uByte6;
    cData[7] = uByte7;
  }
};

#pragma once

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h> //toupper

#define RS232_PARSE_BUFF_SIZE 64

class arduinoSerialMessage
{
  ////rs232 parsing
  //parsing state
  static const uint8_t IDLE         = 0x00;
  static const uint8_t GOT_SW_START = 0x01;
  static const uint8_t GOT_SW_SW1   = 0x02;
  static const uint8_t GOT_SW_SW2   = 0x03;
  static const uint8_t GOT_SW_END   = 0x04;
  //parsing error
  //...

  //
  unsigned int uTemp;
  char printbuf[256];
  char txbuff[256];

public:

  //in
  uint32_t uSW1;
  uint32_t uSW2;

  //out
  uint32_t uRLED;
  uint32_t uGLED;
  uint32_t uBLED;

  //
  std::string create_txmsg_swQuery() {
    sprintf(txbuff, "2AAA3\n");
    return std::string(txbuff);
  }

  //
  std::string create_txmsg_ledControl() {
    sprintf(txbuff, "2%c%c%c3\n", \
            (uRLED + '0'), \
            (uGLED + '0'), \
            (uBLED + '0'));
    return std::string(txbuff);
  }

  int parse_char(char c)
  {
    //static local
    static uint8_t status = arduinoSerialMessage::IDLE;

    //
    switch (status)
    {
    case arduinoSerialMessage::IDLE:
      if ('2' == c) {
        status = arduinoSerialMessage::GOT_SW_START;
      } else {
        //unexpected char. restart.
        status = arduinoSerialMessage::IDLE;
      }
      break;
    case arduinoSerialMessage::GOT_SW_START:
      uSW1 = c - '0';
      status = arduinoSerialMessage::GOT_SW_SW1;
      break;
    case arduinoSerialMessage::GOT_SW_SW1:
      uSW2 = c - '0';
      status = arduinoSerialMessage::GOT_SW_SW2;
      break;
    case arduinoSerialMessage::GOT_SW_SW2:
      if ('3' == c) {
        status = arduinoSerialMessage::GOT_SW_END;
      } else {
        //unexpected char. restart.
        status = arduinoSerialMessage::IDLE;
      }
      break;
    case arduinoSerialMessage::GOT_SW_END: //simply ignore the last character.
      if ('\n' == c) {
        status = arduinoSerialMessage::IDLE;
        return 1; //**i have a new data.
      } else {
        //unexpected char. restart.
        status = arduinoSerialMessage::IDLE;
      }
      break;
    default:
      ;
    }

    return 0; //**data is building... i need more.
  }

  int32_t Print(char * c_str, int32_t length)
  {
    int32_t iStatus;
    uint32_t i;

    iStatus = sprintf(c_str, "(sw1:%d,sw2:%d)[R:%d,G:%d,B:%d]", \
                      uSW1, \
                      uSW2, \
                      uRLED, \
                      uGLED, \
                      uBLED);

    return iStatus;
  }

  int32_t Log(char * c_str, int32_t length, double dTime)
  {
    int32_t iStatus;
    uint32_t i;

    iStatus = sprintf(c_str, "%.6f,%d,%d,%d,%d,%d", \
                      dTime, \
                      uSW1, \
                      uSW2, \
                      uRLED, \
                      uGLED, \
                      uBLED);
    strcat(c_str, "\n");

    return iStatus;
  }

  arduinoSerialMessage & operator =(const arduinoSerialMessage & source)
  {
    //in
    uSW1 = source.uSW1;
    uSW2 = source.uSW2;

    //out
    uRLED = source.uRLED;
    uGLED = source.uGLED;
    uBLED = source.uBLED;

    return (*this);
  }
};

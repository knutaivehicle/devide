extern void DIO_example(uint32_t loopcnt);
extern void CAN_example(uint32_t loopcnt);
extern void GPS_example(uint32_t loopcnt);
extern void SICK_example(uint32_t loopcnt);
extern void M8_example(uint32_t loopcnt);
extern void graphics_example_setup();
extern void graphics_example_draw();

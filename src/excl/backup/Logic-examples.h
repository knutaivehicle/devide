extern void DIO_example(uint32_t loopcnt);
extern void CAN_example(uint32_t loopcnt);
extern void GPS_example(uint32_t loopcnt);
extern void SICK_example(uint32_t loopcnt);
extern void M8_example(uint32_t loopcnt);
extern void M8_example1(uint32_t loopcnt);
extern void graphics_example_setup();
extern void graphics_example_draw();


extern void DIO_CAN_Comm(uint32_t loopcnt);
extern void Signal_Processing(uint32_t loopcnt);
extern void Autonomous_Start_Check(uint32_t loopcnt);
extern void Autonomous_Control_Action(uint32_t loopcnt);

extern void Signal_Processing(uint32_t loopcnt);
extern void Lidar_Object_Tracking(uint32_t loopcnt);
extern void Path_Generation(uint32_t loopcnt);
extern void Infra_Information_Processing(uint32_t loopcnt);
extern void Collision_Estimation(uint32_t loopcnt);
extern void Stop_Check(uint32_t loopcnt);
extern void Output_Signal_Processing(uint32_t loopcnt);

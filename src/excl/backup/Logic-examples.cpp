#include "Main.h"
#include "Logic.h"
#include "Logic-examples.h"

//boost log trivial
#include <boost/log/trivial.hpp>
#define LT_(lvl) BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::lvl))
#define __LOG    BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::info))

//CAN parsers
#include "dataCAN191Message.h"
dataCAN191Message tCAN191;
#include "dataCAN192Message.h"
dataCAN192Message tCAN192;

static __DIO_message_t t_switch;
char Autonomous_Enable;
char Autonomous_Ready;
char System_Fail_Flag;
char Battery_Low_Flag;
char Autonomous_go;
char Stop_Flag;
int Autonomous_Control_Cnt;
char Autonomous_Control_Old;
char Autonomous_Control;
char Autonomous_Start;
char Collision_Risk_Flag;
char Autonomous_Control_Trigger_Flag;
char Autonomous_Control_Trigger_Output;
char Autonomous_Control_Trigger_Cnt;
int Vehicle_Busstop_Distance;
int Vehicle_Speed;
uint32_t Speed_Tx2Center;
int Status_Tx2Center;
uint32_t Veh_Position_x_Tx2Center;
uint32_t Veh_Position_y_Tx2Center;
char temp_Data;


void DIO_CAN_Comm(uint32_t loopcnt)
{
  __CAN_message_t PC2Vehmsg;

  /*
    if(t_switch.uSW1==1)//PC2Vehmsg 0 0 0 0 0 0 0 1
    {
    ;
    }
    else
    {
    ;
    }

    if(t_switch.uSW2==1)//PC2Vehmsg 0 0 0 0 0 0 1 0    // //PC2Vehmsg 0 0 0 0 0 0 1 1
    {
    ;
    }
  */


  if (loopcnt % 3 == 0) { // 60ms
    __CAN_message_buffer_t rxq;
    if (__CANCH1_RX_BUF(rxq)) // ch1 RX
    {
      char canch1buff[256]; //only for formated printing..
      for (uint32_t i = 0; i < rxq.size(); i++) {
        if (rxq[i].lID != 0x1fffffec && rxq[i].lID != 0x1ffffffc)
        {
          rxq[i].Print(canch1buff, 256);
          // __LOG << "[ex:can] : recv. (ch1) : " << std::string(canch1buff);

          //
          tCAN191.parseRxCANMessage(&(rxq[i]));
          char can191buff[500]; //only for formated printing..
          tCAN191.Print(can191buff, 500);
          __LOG << "[ex:can] : 0x191 parsed : " << std::string(can191buff);

          //
          tCAN192.parseRxCANMessage(&(rxq[i]));
          char can192buff[500]; //only for formated printing..
          tCAN192.Print(can192buff, 500);
          __LOG << "[ex:can] : 0x192 parsed : " << std::string(can192buff);
        }
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }


  if (loopcnt % 3 == 0) { // 3 --> 1--->3     60ms

    PC2Vehmsg.type = 'E';         // EXT frame
    PC2Vehmsg.lID = 0x187;        // id 0x100
    PC2Vehmsg.uDlc = 1;           // dlc : 1
    // PC2Vehmsg.cData[0] = loopcnt; // data bytes : [0] XX
    //PC2Vehmsg.cData[0]=t_switch.uSW1 | (Autonomous_Control_Trigger_Output<<1);
    PC2Vehmsg.cData[0]=t_switch.uSW1 | (Autonomous_Control_Trigger_Output<<1)|(temp_Data<<2);
    //
    __CANCH1_TX (PC2Vehmsg);      //STD & EXT send
  }
}



void Signal_Processing(uint32_t loopcnt)
{
  //Vehicle_Busstop_Distance=BusStopPositionCheck(Vehcile_Lat,Vehicle_Lon,BusStop_Lat,BusStop_Lon);
  System_Fail_Flag=0;
  Battery_Low_Flag=0;
  Autonomous_Enable=t_switch.uSW1;
  Autonomous_go=t_switch.uSW2 ;

  Autonomous_Start=0;

  DIO_CAN_Comm(loopcnt);
}

void Autonomous_Start_Check(uint32_t loopcnt)
{

  //=BusStopPositionCheck(Vehcile_Lat,Vehicle_Lon,BusStop_Lat,BusStop_Lon);
  if(Autonomous_Enable==1)
  {
    if(Autonomous_Ready==0)
    {
      if(System_Fail_Flag==0 && Battery_Low_Flag==0 && Autonomous_go==1)
      {
        Autonomous_Ready=1;
        Stop_Flag=1;
      }

    }
    else //Autonomous_Ready==1
    {
      if(System_Fail_Flag==1 || Battery_Low_Flag== 1 || (Autonomous_Control==0 && Autonomous_Control_Old==1) )//|| Autonomous_go==0)
      {
        Autonomous_Ready=0;
        Autonomous_Control_Cnt=0;
      }
      else
      {
        // Test : set
        Autonomous_Start =1;
        Autonomous_Control_Old= Autonomous_Control;
        if(Autonomous_Control==0) //
        {
          if((Autonomous_Start==1 && Stop_Flag==1 && Autonomous_go==1)  || (Collision_Risk_Flag==0 && Stop_Flag==2 && Autonomous_go==1))
          {
            Autonomous_Control_Cnt++;
            if(Autonomous_Control_Cnt >=150)
            {
              Autonomous_Control=1;
              Stop_Flag=0;
            }
            else
            {
              ;
            }
          }
          else
          {
            Autonomous_Control_Cnt=0;
          }
        }
        else // Autonomous_Control==1
        {
          Autonomous_Control_Cnt=0;
          if(Autonomous_Start==0 || Autonomous_go==0)
          {
            Autonomous_Control=0;
          }
          else
          {
            if(Stop_Flag==2)
            {
              Autonomous_Control=0;
            }
            else if(Stop_Flag==1)
            {
              Autonomous_Control=0;
            }
            else if(Autonomous_go==0)
            {
              Autonomous_Control=0;
            }
            else
            {
              ;
            }
          }
        }
      }
    }
  }
  else
  {
    ;
  }
}



void Autonomous_Control_Action(uint32_t loopcnt)
{
  if(Autonomous_Control_Trigger_Flag==0)
  {

    if((Autonomous_Control_Old==0 && Autonomous_Control==1) || (Autonomous_Control_Old==1 && Autonomous_Control==0))

    {
      Autonomous_Control_Trigger_Flag=1;
      Autonomous_Control_Trigger_Output=1;
      Autonomous_Control_Trigger_Cnt=0;
    }
    else
    {
      ;
    }
  }
  else //if(Autonomous_Control_Trigger_Flag==1)
  {
    Autonomous_Control_Trigger_Cnt++;
    if(Autonomous_Control_Trigger_Cnt <=10)
    {
      Autonomous_Control_Trigger_Output=1;
      Autonomous_Control_Trigger_Flag=1;
      if(Autonomous_Control_Old==1)
      {
        Autonomous_Control_Old=0;
      }
    }
    else
    {
      Autonomous_Control_Trigger_Flag=0;
      Autonomous_Control_Trigger_Output=0;
    }
  }
}


void Lidar_Object_Tracking(uint32_t loopcnt)
{

}
void Path_Generation(uint32_t loopcnt)
{

}
void Infra_Information_Processing(uint32_t loopcnt)
{


}
void Collision_Estimation(uint32_t loopcnt)
{


}
void Stop_Check(uint32_t loopcnt)
{
  //Vehicle_Busstop_Distance=BusStopPositionCheck(Vehcile_Lat,Vehicle_Lon,BusStop_Lat,BusStop_Lon);
  Vehicle_Busstop_Distance=1;
  if(Stop_Flag==0)
  {
    if(Collision_Risk_Flag==1)
    {
      Stop_Flag=2;
    }
    else if(Vehicle_Speed== 0 && Vehicle_Busstop_Distance <=1) //Autonomous_start=1ision_Risk_Flag==0
    {
      Stop_Flag=1;
    }
    else
    {
      ;
    }


  }
  else if(Stop_Flag==1)
  {
    if(Autonomous_Control==1)
    {
      Stop_Flag=0;
    }
    else
    {
    }
  }
  else  // Stop_Flag==2
  {
    if(Autonomous_Control==1)
    {
      Stop_Flag=0;
    }
    else
    {
    }
  }
}


void Output_Signal_Processing(uint32_t loopcnt)
{
  //Veh_Position_x_Tx2Center;
  //uint32_t Veh_Position_y_Tx2Center;


  Speed_Tx2Center=Vehicle_Speed;
  Status_Tx2Center=0;
}

void DIO_example(uint32_t loopcnt)
{
  //static __DIO_message_t t_switch;
  


  //
  // **DIO
  //

  // (1)
  { //**DIO_TX_SWQUERY -> DIO_RX_ALL example
    //
    if (loopcnt % 3 == 0) { // 90 ---> 0.03s                0.9s
      __DIO_TX_SWQUERY ();
      __LOG << "[ex:dio] #1 sw stat query sent.";

      __MSLEEP(30); // DIO service loop time == 20ms

      __DIO_message_buffer_t rxq;
      if (__DIO_RX_ALL(rxq))
      {
        __DIO_message_t last = rxq.back();
        __LOG << "[ex:dio] #1 sw stat: (sw1:"
              << last.uSW1
              << "),(sw2:"
              << last.uSW2
              << ")";

        //for test
        t_switch = last;
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  // (2)
  { //**DIO_TX_LEDCTRL example
    //
    if (loopcnt % 100 == 0) { // 1s
      __DIO_message_t led;
      // if (loopcnt % 12 == 0) { // 0.12s
      // if (loopcnt % 5 == 0) { // 50ms // for fun or just for test..
      led.uRLED = t_switch.uSW1;
      led.uGLED = t_switch.uSW2;
      led.uBLED = ((t_switch.uSW1 *2 + t_switch.uSW2) == 3);
      __DIO_TX_LEDCTRL (led);
    }
  }

  // (3)
  { //**DIO_TX_SWQUERY -> DIO_RX_LAST example
    //
    if (loopcnt % 80 == 0) { // 0.8s
      __DIO_TX_SWQUERY ();
      __LOG << "[ex:dio] #2 sw stat query sent.";

      __USLEEP(30); // DIO service loop time == 20ms

      __DIO_message_t last;
      if (__DIO_RX_LAST(last))
      {
        __LOG << "[ex:dio] #2 sw stat: (sw1:"
              << last.uSW1
              << "),(sw2:"
              << last.uSW2
              << ")";
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }
}  

void CAN_example(uint32_t loopcnt)
{
  //
  // **CAN
  //

  // (1)
  { //**CAN_RX example (STD/EXT same)
    //
    if (loopcnt % 3 == 0) { // 60ms
      __CAN_message_buffer_t rxq;
      if (__CANCH1_RX_BUF(rxq)) // ch1 RX
      {
        char canch1buff[256]; //only for formated printing..
        for (uint32_t i = 0; i < rxq.size(); i++) {
          rxq[i].Print(canch1buff, 256);
          __LOG << "[ex:can] : recv. (ch1) : " << std::string(canch1buff);
        }
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  { //**CAN_TX example (STD/EXT -> msg.type = 'S' or 'E')
    //
    if (loopcnt % 3 == 0) { // 60ms
      __CAN_message_t msg;
      msg.type = 'E';         // EXT frame
      msg.lID = 0x100;        // id 0x100
      msg.uDlc = 1;           // dlc : 1
      msg.cData[0] = loopcnt; // data bytes : [0] XX
      //
      __CANCH1_TX (msg);      //STD & EXT send
    }
  }
}

void GPS_example(uint32_t loopcnt)
{
  //
  // **GPS
  //

  // (1)
  { //**GPS_RX example
    //
    if (loopcnt % 110 == 0) { // 1.1s
      __GPS_message_t last;
      if (__GPS_RX_LAST(last)) //
      {
        char gpsbuff[256]; //only for formated printing..
        last.Print(gpsbuff, 256);
        __LOG << "[ex:gps] : gps : " << std::string(gpsbuff);
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  { //**GPS_RX example #2 - DO NOT use.. same but worth performance. probably.
    //__GPS_RX_ALL (__GPS_message_buffer_t & buf)
  }
}

// graphics functions only works in draw() functions!!
// we want to draw this data so made it global.

__SICK_scan_t __example_sick_last;
ofMesh        __example_sick_mesh;

void SICK_example(uint32_t loopcnt)
{
  //
  // **SICK
  //

  // (1)
  { //**SICK_RX example
    //
    if (loopcnt % 5 == 0) { // 50ms
      
      if (__SICK_RX_LAST(__example_sick_last)) {
        __LOG << "[ex:sick] : got a sick scan data.";
        
        // SICK scan's # points are always same. see header file: lms151SCANData.h
        // SICK_RAW_PNTS_MAX - 1082
        // SICK_COV_PNTS_MAX - 541

        //uncomment this to see raw data
        // // console printing of sick data
        // char scanbuff[50000]; //only for formated printing..
        // __example_sick_last.Print(scanbuff, 50000);
        // __LOG << "[ex:sick] : " << std::endl << std::string(scanbuff); // too many

        //CLEAR
        __example_sick_mesh.clear();
        
        //RELOAD
        //graphics for sick scan - preparation (loading)
        //load sick poing cloud
        for (unsigned int pnt = 0; pnt < SICK_COV_PNTS_MAX; pnt++) //iterate over 'points' in the scan
        {
          __example_sick_mesh.addVertex(ofVec3f(__example_sick_last.x[pnt],
                                                __example_sick_last.y[pnt],
                                                __example_sick_last.z[pnt]));
          __example_sick_mesh.addColor(ofFloatColor(0,0,255)); //blue

          // __SICK_scan_t myscaneddata;
         
          // myscaneddata.x[0];
          // myscaneddata.y[0];
          // myscaneddata.z[0];
         
          // myscaneddata.x[10];
          // myscaneddata.y[10];
          // myscaneddata.z[10];
        }
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  { //**SICK_RX example #2 - DO NOT use.. same but worth performance. probably.
    //__SICK_RX_ALL (__SICK_scan_buffer_t & buf)
  }
}

// graphics functions only works in draw() functions!!
// we want to draw this data so made it global.

__M8_scan_t __example_m8_last;
ofMesh      __example_m8_mesh;

void M8_example(uint32_t loopcnt)
{
  //
  // **M8 (M8_FL == M8_FR)
  //

  // (1)
  { //**M8_RX example
    //
    if (loopcnt % 11 == 0) { // 110ms

      if (__M8_FL_RX_LAST(__example_m8_last)) {
        __LOG << "[ex:m8] : m8 #of points : " << __example_m8_last.size();

        //mesh update
        
        //CLEAR
        __example_m8_mesh.clear();
        
        //RELOAD
        for (unsigned int pnt = 0; pnt < __example_m8_last.size(); pnt++) //iterate over 'points' in the scan
        {
          // uncomment this to see raw data but.. be prepared. too many data.
              
          // //console printing of m8 data
          // __LOG << "[ex:m8] : m8 data-x:" << __example_m8_last[pnt].x
          //       << ",y:" << __example_m8_last[pnt].y
          //       << ",z:" << __example_m8_last[pnt].z
          //       << ",i:" << __example_m8_last[pnt].i
          //       << ",r:" << __example_m8_last[pnt].r;

          //graphics for m8 scan - preparation (loading)
            
          //load m8 poing cloud
          __example_m8_mesh.addVertex(ofVec3f(__example_m8_last[pnt].x, __example_m8_last[pnt].y, __example_m8_last[pnt].z));
            
          // ring coloring
          switch (__example_m8_last[pnt].r)
          {
          case 0:
            __example_m8_mesh.addColor(ofFloatColor(255,0,0)); //red
            break;
          case 1:
            __example_m8_mesh.addColor(ofFloatColor(0,255,0)); //green
            break;
          case 2:
            __example_m8_mesh.addColor(ofFloatColor(0,0,255)); //blue
            break;
          case 3:
            __example_m8_mesh.addColor(ofFloatColor(255,255,0)); //yellow
            break;
          case 4:
            __example_m8_mesh.addColor(ofFloatColor(0,255,255)); //cyan
            break;
          case 5:
            __example_m8_mesh.addColor(ofFloatColor(255,0,255)); //magenta
            break;
          case 6:
            __example_m8_mesh.addColor(ofFloatColor(255,255,255)); //white
            break;
          case 7:
            __example_m8_mesh.addColor(ofFloatColor(0,0,0)); //black
            break;
          default:
            ;
          }
        }
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }
}

__M8_scan_t __example_m8_last1;
ofMesh      __example_m8_mesh1;

void M8_example1(uint32_t loopcnt)
{
  //
  // **M8 (M8_FL == M8_FR)
  //

  // (1)
  { //**M8_RX example
    //
    if (loopcnt % 11 == 0) { // 110ms

      if (__M8_FR_RX_LAST(__example_m8_last1)) {
        __LOG << "[ex:m8] : m8 #of points : " << __example_m8_last1.size();

        //mesh update
        
        //CLEAR
        __example_m8_mesh1.clear();
        
        //RELOAD
        for (unsigned int pnt = 0; pnt < __example_m8_last1.size(); pnt++) //iterate over 'points' in the scan
        {
          // uncomment this to see raw data but.. be prepared. too many data.
              
          // //console printing of m8 data
          // __LOG << "[ex:m8] : m8 data-x:" << __example_m8_last1[pnt].x
          //       << ",y:" << __example_m8_last1[pnt].y
          //       << ",z:" << __example_m8_last1[pnt].z
          //       << ",i:" << __example_m8_last1[pnt].i
          //       << ",r:" << __example_m8_last1[pnt].r;

          //graphics for m8 scan - preparation (loading)
            
          //load m8 poing cloud
          __example_m8_mesh1.addVertex(ofVec3f(__example_m8_last1[pnt].x, __example_m8_last1[pnt].y, __example_m8_last1[pnt].z));
            
          // ring coloring
          switch (__example_m8_last1[pnt].r)
          {
          case 0:
            __example_m8_mesh1.addColor(ofFloatColor(255,0,0)); //red
            break;
          case 1:
            __example_m8_mesh1.addColor(ofFloatColor(0,255,0)); //green
            break;
          case 2:
            __example_m8_mesh1.addColor(ofFloatColor(0,0,255)); //blue
            break;
          case 3:
            __example_m8_mesh1.addColor(ofFloatColor(255,255,0)); //yellow
            break;
          case 4:
            __example_m8_mesh1.addColor(ofFloatColor(0,255,255)); //cyan
            break;
          case 5:
            __example_m8_mesh1.addColor(ofFloatColor(255,0,255)); //magenta
            break;
          case 6:
            __example_m8_mesh1.addColor(ofFloatColor(255,255,255)); //white
            break;
          case 7:
            __example_m8_mesh1.addColor(ofFloatColor(0,0,0)); //black
            break;
          default:
            ;
          }
        }
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }
}

//**
// *********!!! REMEMBER call each mesh's draw() in Logic's draw() !!!
//

// all of* is from openFrameworks graphics engine.

// link: reference : http://openframeworks.cc/documentation/
// link: reading   : http://openframeworks.cc/ofBook/chapters/foreword.html

void graphics_example_setup()
{
  __example_sick_mesh.setMode(OF_PRIMITIVE_POINTS);
  __example_m8_mesh.setMode(OF_PRIMITIVE_POINTS);
  __example_m8_mesh1.setMode(OF_PRIMITIVE_POINTS);
}

void graphics_example_draw()
{
  __example_sick_mesh.draw();
  __example_m8_mesh.draw();
  __example_m8_mesh1.draw();
}

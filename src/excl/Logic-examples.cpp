#include "Main.h"
#include "Logic.h"
#include "Logic-examples.h"

//boost log trivial
#include <boost/log/trivial.hpp>
#define LT_(lvl) BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::lvl))
#define __LOG    BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::info))

void DIO_example(uint32_t loopcnt)
{
  //
  // **DIO
  //

  // (1)
  { //**DIO_TX_LEDCTRL example
    //
    if (loopcnt % 100 == 0) { // 1s
      __DIO_message_t led;
      // if (loopcnt % 12 == 0) { // 0.12s
      // if (loopcnt % 5 == 0) { // 50ms // for fun or just for test..
      led.uRLED = (int)ofRandom(2);
      led.uGLED = (int)ofRandom(2);
      led.uBLED = (int)ofRandom(2);
      __DIO_TX_LEDCTRL (led);
    }
  }

  // (2)
  { //**DIO_TX_SWQUERY -> DIO_RX_ALL example
    //
    if (loopcnt % 90 == 0) { // 0.9s
      __DIO_TX_SWQUERY ();
      __LOG << "[ex:dio] #1 sw stat query sent.";

      __USLEEP(30); // DIO service loop time == 20ms

      __DIO_message_buffer_t rxq;
      if (__DIO_RX_ALL(rxq))
      {
        __DIO_message_t last = rxq.back();
        __LOG << "[ex:dio] #1 sw stat: (sw1:"
              << last.uSW1
              << "),(sw2:"
              << last.uSW2
              << ")";
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  // (3)
  { //**DIO_TX_SWQUERY -> DIO_RX_LAST example
    //
    if (loopcnt % 80 == 0) { // 0.8s
      __DIO_TX_SWQUERY ();
      __LOG << "[ex:dio] #2 sw stat query sent.";

      __USLEEP(30); // DIO service loop time == 20ms

      __DIO_message_t last;
      if (__DIO_RX_LAST(last))
      {
        __LOG << "[ex:dio] #2 sw stat: (sw1:"
              << last.uSW1
              << "),(sw2:"
              << last.uSW2
              << ")";
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }
}  

void CAN_example(uint32_t loopcnt)
{
  //
  // **CAN
  //

  // (1)
  { //**CAN_RX example (STD/EXT same)
    //
    if (loopcnt % 70 == 0) { // 0.7s
      __CAN_message_buffer_t rxq;
      if (__CANCH1_RX_BUF(rxq)) // ch1 RX
      {
        char canch1buff[256]; //only for formated printing..
        for (uint32_t i = 0; i < rxq.size(); i++) {
          rxq[i].Print(canch1buff, 256);
          __LOG << "[ex:can] : recv. (ch1) : " << std::string(canch1buff);
        }
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  { //**CAN_TX example (STD/EXT -> msg.type = 'S' or 'E')
    //
    if (loopcnt % 60 == 0) { // 0.6s
      __CAN_message_t msg;
      msg.type = 'E';         // EXT frame
      msg.lID = 0x100;        // id 0x100
      msg.uDlc = 1;           // dlc : 1
      msg.cData[0] = loopcnt; // data bytes : [0] XX
      //
      __CANCH2_TX (msg);      //STD & EXT send
    }
  }
}

void GPS_example(uint32_t loopcnt)
{
  //
  // **GPS
  //

  // (1)
  { //**GPS_RX example
    //
    if (loopcnt % 110 == 0) { // 1.1s
      __GPS_message_t last;
      if (__GPS_RX_LAST(last)) //
      {
        char gpsbuff[256]; //only for formated printing..
        last.Print(gpsbuff, 256);
        __LOG << "[ex:gps] : gps : " << std::string(gpsbuff);
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  { //**GPS_RX example #2 - DO NOT use.. same but worth performance. probably.
    //__GPS_RX_ALL (__GPS_message_buffer_t & buf)
  }
}

// graphics functions only works in draw() functions!!
// we want to draw this data so made it global.

__SICK_scan_t __example_sick_last;
ofMesh        __example_sick_mesh;

void SICK_example(uint32_t loopcnt)
{
  //
  // **SICK
  //

  // (1)
  { //**SICK_RX example
    //
    if (loopcnt % 5 == 0) { // 50ms
      
      if (__SICK_RX_LAST(__example_sick_last)) {
        __LOG << "[ex:sick] : got a sick scan data.";
        
        // SICK scan's # points are always same. see header file: lms151SCANData.h
        // SICK_RAW_PNTS_MAX - 1082
        // SICK_COV_PNTS_MAX - 541

        //uncomment this to see raw data
        // console printing of sick data
        char scanbuff[50000]; //only for formated printing..
        __example_sick_last.Print(scanbuff, 50000);
        __LOG << "[ex:sick] : " << std::endl << std::string(scanbuff); // too many

        //CLEAR
        __example_sick_mesh.clear();
        
        //RELOAD
        //graphics for sick scan - preparation (loading)
        //load sick poing cloud
        for (unsigned int pnt = 0; pnt < SICK_COV_PNTS_MAX; pnt++) //iterate over 'points' in the scan
        {
          __example_sick_mesh.addVertex(ofVec3f(__example_sick_last.x[pnt],
                                                __example_sick_last.y[pnt],
                                                __example_sick_last.z[pnt]));
          __example_sick_mesh.addColor(ofFloatColor(0,0,255)); //blue
        }
      }
      else
      {
        ;// Come back next time! (No data available)
      }
    }
  }

  { //**SICK_RX example #2 - DO NOT use.. same but worth performance. probably.
    //__SICK_RX_ALL (__SICK_scan_buffer_t & buf)
  }
}

// graphics functions only works in draw() functions!!
// we want to draw this data so made it global.

__M8_scan_t __example_m8_last;
ofMesh      __example_m8_mesh;

void M8_example(uint32_t loopcnt)
{
  //
  // **M8 (M8_FL == M8_FR)
  //

  // (1)
  { //**M8_RX example
    //
    if (loopcnt % 11 == 0) { // 110ms

      if (__M8_FL_RX_LAST(__example_m8_last)) {
        __LOG << "[ex:m8] : m8 #of points : " << __example_m8_last.size();

        //mesh update
        
        //CLEAR
        __example_m8_mesh.clear();
        
        //RELOAD
        for (unsigned int pnt = 0; pnt < __example_m8_last.size(); pnt++) //iterate over 'points' in the scan
        {
          // uncomment this to see raw data but.. be prepared. too many data.
              
          // //console printing of m8 data
          // __LOG << "[ex:m8] : m8 data-x:" << __example_m8_last[pnt].x
          //       << ",y:" << __example_m8_last[pnt].y
          //       << ",z:" << __example_m8_last[pnt].z
          //       << ",i:" << __example_m8_last[pnt].i
          //       << ",r:" << __example_m8_last[pnt].r;

          //graphics for m8 scan - preparation (loading)
            
          //load m8 poing cloud
          __example_m8_mesh.addVertex(ofVec3f(__example_m8_last[pnt].x, __example_m8_last[pnt].y, __example_m8_last[pnt].z));
            
          // ring coloring
          switch (__example_m8_last[pnt].r)
          {
          case 0:
            __example_m8_mesh.addColor(ofFloatColor(255,0,0)); //red
            break;
          case 1:
            __example_m8_mesh.addColor(ofFloatColor(0,255,0)); //green
            break;
          case 2:
            __example_m8_mesh.addColor(ofFloatColor(0,0,255)); //blue
            break;
          case 3:
            __example_m8_mesh.addColor(ofFloatColor(255,255,0)); //yellow
            break;
          case 4:
            __example_m8_mesh.addColor(ofFloatColor(0,255,255)); //cyan
            break;
          case 5:
            __example_m8_mesh.addColor(ofFloatColor(255,0,255)); //magenta
            break;
          case 6:
            __example_m8_mesh.addColor(ofFloatColor(255,255,255)); //white
            break;
          case 7:
            __example_m8_mesh.addColor(ofFloatColor(0,0,0)); //black
            break;
          default:
            ;
          }
        }
      }
    }
    else
    {
      ;// Come back next time! (No data available)
    }
  }
}

//**
// *********!!! REMEMBER call each mesh's draw() in Logic's draw() !!!
//

// all of* is from openFrameworks graphics engine.

// link: reference : http://openframeworks.cc/documentation/
// link: reading   : http://openframeworks.cc/ofBook/chapters/foreword.html

void graphics_example_setup()
{
  __example_sick_mesh.setMode(OF_PRIMITIVE_POINTS);
  __example_m8_mesh.setMode(OF_PRIMITIVE_POINTS);
}

void graphics_example_draw()
{
  __example_sick_mesh.draw();
  __example_m8_mesh.draw();
}

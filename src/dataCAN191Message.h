#pragma once

#include <stdint.h>
#include "i7530aCANMessage.h"

struct dataCAN191Message
{
  uint8_t uAccel_On;
  uint8_t uBrake_On;
  uint8_t uAutoMode;
  uint8_t uFW_SW;
  uint8_t uBW_SW;
  uint8_t uSP_5;
  uint8_t uSP_6;
  uint8_t uSP_7;
  uint8_t uSP_8;
  uint8_t uSP_9;
  uint8_t uSP_10;
  uint8_t uPCcon_Speed;
  uint8_t uPCcon_ST;
  uint8_t uPCcon_En;

  uint16_t uIndSen_L;
  uint16_t uIndSen_R;
  uint16_t uSonic_L;
  uint16_t uSonic_R;
  uint16_t uThrottle;
  uint16_t uMagnet;
  uint16_t uFltCode;
  uint16_t uSpeed;

  void parseRxCANMessage(i7530aCANMessage * ptRxMsg)
  {
    //(((int32_t)(ptRxMsg->cData[2]&0xF8))>>3)

    if (ptRxMsg->lID == 0x191)
    {
      if (ptRxMsg->cData[1] == 0x80) //Frame #1
      {
        //byte 0
        uPCcon_En    = (((int32_t)(ptRxMsg->cData[2]&0x01))>>0);
        uPCcon_ST    = (((int32_t)(ptRxMsg->cData[2]&0x02))>>1);
        uPCcon_Speed = (((int32_t)(ptRxMsg->cData[2]&0x1C))>>2); // 0001 1100 : 1C
        uAccel_On    = (((int32_t)(ptRxMsg->cData[2]&0x20))>>5);
        uBrake_On    = (((int32_t)(ptRxMsg->cData[2]&0x40))>>6);
        uAutoMode    = (((int32_t)(ptRxMsg->cData[2]&0x80))>>7);

        //byte 1
        uFW_SW       = (((int32_t)(ptRxMsg->cData[3]&0x01))>>0);
        uBW_SW       = (((int32_t)(ptRxMsg->cData[3]&0x02))>>1);
        uSP_5        = (((int32_t)(ptRxMsg->cData[3]&0x04))>>2);
        uSP_6        = (((int32_t)(ptRxMsg->cData[3]&0x08))>>3);
        uSP_7        = (((int32_t)(ptRxMsg->cData[3]&0x10))>>4);
        uSP_8        = (((int32_t)(ptRxMsg->cData[3]&0x20))>>5);
        uSP_9        = (((int32_t)(ptRxMsg->cData[3]&0x40))>>6);
        uSP_10       = (((int32_t)(ptRxMsg->cData[3]&0x80))>>7);

        uIndSen_L    = (int32_t)((((int32_t)(ptRxMsg->cData[5]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[4]&0xFF))>>0));
        uIndSen_R    = (int32_t)((((int32_t)(ptRxMsg->cData[7]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[6]&0xFF))>>0));
      }
      else if (ptRxMsg->cData[1] == 0x40) //Frame #2
      {
        uSonic_L     = (int32_t)((((int32_t)(ptRxMsg->cData[3]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[2]&0xFF))>>0));
        uSonic_R     = (int32_t)((((int32_t)(ptRxMsg->cData[5]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[4]&0xFF))>>0));
        uThrottle    = (int32_t)((((int32_t)(ptRxMsg->cData[7]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[6]&0xFF))>>0));
      }
      else if (ptRxMsg->cData[1] == 0x20) //Frame #2
      {
        uMagnet      = (int32_t)((((int32_t)(ptRxMsg->cData[3]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[2]&0xFF))>>0));
        uFltCode     = (int32_t)((((int32_t)(ptRxMsg->cData[5]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[4]&0xFF))>>0));
        uSpeed       = (int32_t)((((int32_t)(ptRxMsg->cData[7]&0xFF))<<8) | (((int32_t)(ptRxMsg->cData[6]&0xFF))>>0));
      }
    }
  }

  int32_t Print(char * c_str, int32_t length){
    int32_t bResult;

    bResult = sprintf(c_str, "uPCcon_En:%d,uPCcon_ST:%d,uPCcon_Speed:%d,uAccel_On:%d,uBrake_On:%d,uAutoMode:%d,uFW_SW:%d,uBW_SW:%d,uSP_5:%d,uSP_6:%d,uSP_7:%d,uSP_8:%d,uSP_9:%d,uSP_10:%d,uIndSen_L:%d,uIndSen_R:%d,uSonic_L:%d,uSonic_R:%d,uThrottle:%d,uMagnet:%d,uFltCode:%d,uSpeed:%d\n",
                      uPCcon_En,
                      uPCcon_ST,
                      uPCcon_Speed,
                      uAccel_On,
                      uBrake_On,
                      uAutoMode,
                      uFW_SW,
                      uBW_SW,
                      uSP_5,
                      uSP_6,
                      uSP_7,
                      uSP_8,
                      uSP_9,
                      uSP_10,
                      uIndSen_L,
                      uIndSen_R,
                      uSonic_L,
                      uSonic_R,
                      uThrottle,
                      uMagnet,
                      uFltCode,
                      uSpeed);

    return bResult;
  }
};

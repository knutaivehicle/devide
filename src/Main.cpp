#include "Main.h" //openframeworks app

//** intentionally, not utilizing ofThread. all threading will go with boost::thread
//** intentionally, not utilizing ofLog. all loggin will be done with boost::log way

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> //mkdir
#include <sys/stat.h>

//logic
#include "Logic.h"                  // user logic
#include "Logic-main.h"             // user logic

// (0) one-time runner for preparation
void Main::setup() {

  //draw setup
  ofSetFrameRate(20); // 50 fps == 20 ms
  //
  ofSetVerticalSync(true);
  ofEnableDepthTest();
  glEnable(GL_POINT_SMOOTH);
  glPointSize(3);
  //
  __LOGIC.setup(); // << USER CODE >>

  //// << service threads start >> ////
  
  // arduino dio - 20ms
  __DIO.config_parameters("DIO", "/dev/arduino", 9600);
  __DIO.start();

  // can ch1 thread - 20ms
  __CANCH1.config_parameters("CANch1", "/dev/canch1", 115200);;
  __CANCH1.start();

  // can ch2 thread - 20ms
  __CANCH2.config_parameters("CANch2", "/dev/canch2", 115200);
  //__CANCH2.start();

  // gps thread - 20ms
  __GPS.config_parameters("GPS", "/dev/gps", 115200);
  __GPS.start();

  // SICK scanner thread - 20ms~
  __SICK.config_parameters("SICK", "192.168.2.2", 2111);
  __SICK.start();

  // M8 front left scanner thread - 80ms
  __M8_FL.center_x = 0;
  __M8_FL.center_y = 0;
  __M8_FL.rotation_deg = 0;
  __M8_FL.config_parameters("M8FL", "192.168.1.3", 4141);
  __M8_FL.start();
  
  // M8 front right scanner thread - 80ms
  __M8_FR.center_x = 0;
  __M8_FR.center_y = 0;
  __M8_FR.rotation_deg = 0;
  __M8_FR.config_parameters("M8FR", "192.168.3.2", 4141);
  __M8_FR.start();
  
  // Control center communication thread - ms
  __CCENTER.config_parameters("CCENTER", "222.116.156.185", 58888, "222.116.156.184", 55558, "222.116.156.185", 60000); // (tcp_listen), (tcp_connect), (udp_server)
  __CCENTER.start();
  
  //logic thread start
  __LOGIC.start();

}

// cleanup
void Main::exit() {

  //logic exit.
  __LOGIC.exit();
  
  // ARDUINO dio thread stop
  __DIO.stop();

  // SICK scanner thread stop
  __SICK.stop();

  // M8 front left scanner thread
  __M8_FL.stop();
  
  // M8 front left scanner thread
  __M8_FR.stop();
  
  // can ch1 thread stop
  __CANCH1.stop();

  // can ch2 thread stop
  __CANCH2.stop();

  // gps thread stop
  __GPS.stop();

  //control center thread stop
  __CCENTER.stop();
  
  //logic thread stop
  __LOGIC.stop();
  
}

// (1) update
void Main::update() {
  ;
}

// (2) visualize
void Main::draw() {

  cam.begin();
  //
  ofScale(100, 100, 100); // zoom 100x
  //
  __LOGIC.draw();
  //
  cam.end();
  
  __LOGIC.overlay();
}

void Main::startLogging() {

  //create folder
  //build a timestring of the moment
  char outstr[200];
  time_t t;
  struct tm *tmp;

  t = time(NULL);
  tmp = localtime(&t);
  if (tmp == NULL) {
    perror("[main] localtime error");
    return;
  }

  if (strftime(outstr, sizeof(outstr), "logdata-%Y-%m-%d-%H-%M-%S-%a", tmp) == 0) {
    perror("[main] strftime error");
    return;
  }
  LT_(info) << "[main] log name -> " << std::string(outstr);

  //
  char where[512];
  getcwd(where, 512);
  LT_(info) << "[main] current path -> " << std::string(where);

  //
  char folder[512];
  folder[0] = '\0';
  strcat(folder, where);
  strcat(folder, "/");
  strcat(folder, outstr);
  strcat(folder, "/");
  LT_(info) << "[main] log path -> " << std::string(folder);

  //mkdir
  if (0 == mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))
  {

    //
    __DIO.logStart(std::string(folder));
    __SICK.logStart(std::string(folder));
    M8_FL_logstart(std::string(folder)); //from Logic-main.cpp/h
    M8_FR_logstart(std::string(folder)); //from Logic-main.cpp/h
    __CANCH1.logStart(std::string(folder));
    __CANCH2.logStart(std::string(folder));
    __GPS.logStart(std::string(folder));
    __CCENTER.logStart_ipcam(std::string(folder));
    __CCENTER.logStart_control(std::string(folder));

  } else {
    //failed to create the folder!
  }
}

void Main::stopLogging() {
  
  __DIO.logStop();
  __SICK.logStop();
  M8_FL_logstop(); //from Logic-main.cpp/h
  M8_FR_logstop(); //from Logic-main.cpp/h
  __CANCH1.logStop();
  __CANCH2.logStop();
  __GPS.logStop();
  __CCENTER.logStop_ipcam();
  __CCENTER.logStop_control();
  
}

void Main::keyPressed(int key)
{
  switch(toupper(key))
  {
  case 'L':
    startLogging();
    break;
  case 'K':
    stopLogging();
    break;
  default:
    ;
  }
}

//------------------------------------------------
//cpp entry point (global)
int main( ){

#ifdef OF_APP_GRAPHICS
  
  //CHOICES: OF_WINDOW or OF_FULLSCREEN
  ofSetupOpenGL(1280, 800, OF_WINDOW);
  //main graphic thread runner
  ofRunApp(new Main());
  
#else

  ofAppNoWindow nowindow;
  ofSetupOpenGL(&nowindow, 0, 0, OF_WINDOW);
  ofRunApp(new Main());
  
#endif

}

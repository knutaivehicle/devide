#include "lms151SICKInterface.h"

////system
#include <stdio.h> //sscanf
#include <string.h> //strlen
#include <sys/select.h> //select, FD_SET, FD_ZERO
#include <sys/time.h> //timeval
#include <sys/types.h>
#include <unistd.h> //read, write
#include <sys/socket.h> //socket
#include <netinet/in.h> //inet_addr
#include <arpa/inet.h> //htons

//boost
//boost log trivial
#include <boost/log/trivial.hpp> // (include only in .cpp)
#define LT_(lvl) BOOST_LOG_TRIVIAL(lvl)

//
#include "hrTimestamp.h"

void lms151SICKInterface::start()
{
  LT_(info) << "[sick] start thread.";
  b_is_running = true;
  thrd = boost::thread (&lms151SICKInterface::threadedFunction, this);
}

void lms151SICKInterface::stop()
{
  LT_(info) << "[sick] stop thread.";
  b_is_running = false;
  //**thread join doesn't work nicely. or just takes too long.
  thrd.join();
  LT_(info) << "[sick] thread joined.";
}

void lms151SICKInterface::logStart(std::string strLogFileName_)
{
  if (bIsLogging == false) {
    strLogFileName = strLogFileName_ + s_name + ".csv";
    bIsStartLogReq = true;
    LT_(info) << "[" << s_name << "] logging start:" << strLogFileName;
  }
}

void lms151SICKInterface::logStop()
{
  if (bIsLogging == true) {
    bIsStartLogReq = false;
    LT_(info) << "[" << s_name << "] logging stop.";
  }
}

uint32_t lms151SICKInterface::config_parameters(std::string s_name_, std::string s_ip_, unsigned int u_port_)
{
  //device name
  s_name = s_name_;
  s_ip = s_ip_;
  u_port = u_port_;

  //shared queue
  rxq.clear();
  
  return 0;
}

void lms151SICKInterface::threadedFunction()
{
  // get configuration
  int scaningFrequency = 0;
  int angleResolution = 0;
  int startAngle = 0;
  int stopAngle = 0;
  
  // set configuration
  int outputChannel = 1;
  int remission = 1;
  int resolution = 1; 
  int encoder = 0; 
  int position = 0;        //16bit resolution
  int deviceName = 0; 
  int timestamp = 0; 
  int outputInterval = 1;  // every scan

  // start measure
  int start  =  0;
  
  // socket
  int sockfd;

  // misc.
  char buf[100];
  int len = 0;
  int ret = 0;

  //print out configuration info. to the console.
  LT_(info) << "[sick" << s_ip << "] config: s_ip: " << s_ip;
  LT_(info) << "[sick" << s_ip << "] config: u_port: " << u_port;

  //logging
  FILE * fpLog;
  char logbuff[50000];
  hrTimestamp tstamp;

  ////start SICK Lidar scan
  bStarted = false;

  // connect socket
  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = inet_addr(s_ip.c_str());
  address.sin_port = htons(u_port);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (!sockfd) {
    LT_(error) << "[sick] " << strerror(errno);
    return;
  }
  LT_(info) << "[sick] Connecting socket..";
  ret = connect(sockfd, (struct sockaddr *) &address, sizeof(address));
  if (ret) {
    LT_(error) << "[sick] " << strerror(errno);
    return;
  }
  sprintf(buf, "%c%s%c", 0x02, "sMN SetAccessMode 03 F4724744", 0x03);
  struct timeval timeout;
  fd_set rfds;
  int result = 0;
  do //loop until data is available to read
  {
    //a procedure for response of the device
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    write(sockfd, buf, strlen(buf));

    FD_ZERO(&rfds);
    FD_SET(sockfd, &rfds);
    result = select(sockfd + 1, &rfds, NULL, NULL, &timeout);
  }
  while (result <= 0);

  //login message
  len = read(sockfd, buf, 100);
  if (buf[0] != 0x02) {
    LT_(error) << "[sick] invalid packet recieved";
    close(sockfd);
    return;
  }
  buf[len] = 0;
  LT_(info) << "[sick] Login RX: " << buf;

  //configuration update
  sprintf(buf, "%c%s %02X 00 %d %d 0 %02X 00 %d %d 0 %d +%d%c", \
          0x02,
          "sWN LMDscandatacfg", \
          outputChannel, \
          remission, \
          resolution, \
          encoder, \
          position, \
          deviceName, \
          timestamp, \
          outputInterval, \
          0x03);
  LT_(info) << "[sick] Scan data configure TX: " << buf;
  write(sockfd, buf, strlen(buf));
  len = read(sockfd, buf, 100);
  if (buf[0] != 0x02) {
    LT_(error) << "[sick] Query invalid packet recieved";
    close(sockfd);
    return;
  }
  buf[len] = '\0';
  LT_(info) << "[sick] Set scandata configure RX: " << buf;

  usleep(5);

  //start measurement
  sprintf(buf, "%c%s%c", 0x02, "sMN LMCstartmeas", 0x03);
  write(sockfd, buf, strlen(buf));
  len = read(sockfd, buf, 100);
  if (buf[0] != 0x02) {
    LT_(error) << "[sick] invalid packet recieved";
    close(sockfd);
    return;
  }
  buf[len] = 0;
  LT_(info) << "[sick] start meas. RX: " << buf;
  do
  {
    // query status
    sprintf(buf, "%c%s%c", 0x02, "sRN STlms", 0x03);
    write(sockfd, buf, strlen(buf));
    len = read(sockfd, buf, 100);
    if (buf[0] != 0x02) {
      LT_(error) << "[sick] Query invalid packet recieved";
      close(sockfd);
      return;
    }
    buf[len] = 0;
    LT_(info) << "[sick] Query RX: " << buf;
    sscanf((buf + 10), "%d", &ret);
    LT_(info) << "[sick] LMS status is " << ret;
    switch(ret)
    {
    case 1:
      LT_(info) << "[sick] Initialization";
      break;
    case 7:
      LT_(info) << "[sick] LMS1xx Ready for measurement. ok";
      break;
    case 6:
      LT_(info) << "[sick] Ready";
      break;
    case 3:
      LT_(info) << "[sick] IDLE";
      break;
    case 4:
      LT_(info) << "[sick] Rotate";
      break;
    default:
      break;
    }
  } while (ret != 7); //READY_FOR_MEAS == 7

  // get SICK configurations
  LT_(info) << "[sick] ============================ Get SICK scan configuration ============================";
  sprintf(buf, "%c%s%c", 0x02, "sRN LMPscancfg", 0x03);
  write(sockfd, buf, strlen(buf));
  len = read(sockfd, buf, 100);

  if (buf[0] != 0x02) {
    LT_(error) << "[sick] get configuration invalid packet recieved";
    close(sockfd);
    return;
  }
  buf[len] = 0;
  LT_(info) << "[sick] Get configure RX: " << buf;
  sscanf(buf + 1, "%*s %*s %X %*d %X %X %X", \
         &scaningFrequency, \
         &angleResolution, \
         &startAngle, \
         &stopAngle);
  LT_(info) << "[sick] Freq.=" << scaningFrequency/100 << ", Resolution=" << angleResolution*1.0/10000 \
                << " startAngle=" << startAngle/10000 << ", stopAngle=" << stopAngle/10000;
  LT_(info) << "[sick] ============================ SICK scan configuration END ============================";

  // start device
  sprintf(buf, "%c%s%c", 0x02, "sMN Run", 0x03);
  write(sockfd, buf, strlen(buf));
  len = read(sockfd, buf, 100);
  if (buf[0] != 0x02) {
    LT_(error) << "[sick] invalid packet recieved";
    close(sockfd);
    return;
  }
  buf[len] = 0;
  LT_(info) << "[sick] Run RX: " << buf;

  // start read data
  start = 1;
  sprintf(buf, "%c%s %d%c", 0x02, "sEN LMDscandata", start, 0x03);
  write(sockfd, buf, strlen(buf));
  len = read(sockfd, buf, 100);
  if (buf[0] != 0x02) {
    LT_(error) << "[sick] scan data invalid packet recieved";
    close(sockfd);
    return;
  }
  buf[len-1] = 0;
  LT_(info) << "[sick] Scan data cont'. RX: " << buf;

  //
  // int devicestatus = -1; //for logging.. //probably to check out if scanner is running..or not.
  
  while (b_is_running == true)
  {
    //logging - start
    if (bIsLogging == false && bIsStartLogReq == true) {
      bIsStartLogReq = false;
      fpLog = fopen(strLogFileName.c_str(), "w");
      if (fpLog != NULL) {
        bIsLogging = true;
      }
    }

    //read measurement
    char rxbuff[10000]; // in general, less than 5000 characters. in a single scan.
    int rxidx = 0;
    int len=0;
    char c;
    do
    { 
      len = read(sockfd, &c, 1);
      rxbuff[rxidx++] = c;
    } while (c != 0x03);
    rxbuff[rxidx] = '\0';

    //parse and queue
    if (len > 0)
    {
      lms151SCANData new_scan;
      new_scan.center_x = 0;
      new_scan.center_y = 0;
      new_scan.rotation_deg = 0;
      // LT_(info) << "[sick] " << rxbuff; // to print out raw rxbuff.
      new_scan.parse_scan(rxbuff);
      mtx_rxq.lock();
      rxq.push_back(new_scan);
      mtx_rxq.unlock();

      //logging - write one by one
      if (bIsLogging == true) {
        tstamp.get_now();
        new_scan.Log(logbuff, 50000, tstamp.d_time);
        fprintf(fpLog, logbuff);
        fflush(fpLog);
      }
    }

    //logging - end
    if (bIsLogging == true && bIsStopLogReq == true) {
      bIsStopLogReq = false;
      fflush(fpLog);
      fclose(fpLog);
      bIsLogging = false;
    }
    
    //
    boost::this_thread::sleep_for(boost::chrono::milliseconds(5)); //5ms
  }

  //stop the device
  do 
  {
    sprintf(buf, "%c%s %d%c", 0x02, "sEN LMDscandata", 0x00, 0x03);
    write(sockfd, buf, strlen(buf));
    len = read(sockfd, buf, 100);
  } while (buf[0] != 0x02);
  LT_(info) << "[sick] Stop Scan data cont'. RX: " << buf;

  sprintf(buf, "%c%s%c", 0x02, "sMN LMCstopmeas", 0x03);
  write(sockfd, buf, strlen(buf));
  sleep(100); //wait for 100ms
  len = read(sockfd, buf, 100);
  buf[len] = 0;
  LT_(info) << "[sick] Stop meas. RX: " << buf;

  //close socket
  close(sockfd);
  LT_(info) << "[sick] User stop the program";

  return;
}

//ctor
lms151SICKInterface::lms151SICKInterface() {

  //
  b_is_running = false;

  //
  bStarted = false;

  //
  bIsLogging = false;
  bIsStartLogReq = false;
  bIsStopLogReq = false;
  strLogFileName = "";
  
  //default ip/port preset
  s_ip = std::string("192.168.2.2");
  u_port = 2111;
}

//dtor
lms151SICKInterface::~lms151SICKInterface() {
  
  //
  if (bIsLogging == true)
  {
    logStop();
  }

  //
  if (b_is_running == true)
  {
    stop();
  }

  if (bStarted == true) {
    
    ; //reset obj
    
    bStarted = false;
  }
}

//
// **global instance/interface
//

////**instance (initialization)
lms151SICKInterface __SICK;

////**methods

//
//** RX series return values
//
//   - 0 : no data to get
//   - 0 : yes data to get
//

//RX
int __SICK_RX_ALL (__SICK_scan_buffer_t & buf)
{
  __SICK.mtx_rxq.lock();
  if (__SICK.rxq.empty()) {
    __SICK.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    buf = __SICK.rxq; // capture whole buf
    __SICK.rxq.clear();
    __SICK.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//RX
int __SICK_RX_LAST (__SICK_scan_t & last)
{
  __SICK.mtx_rxq.lock();
  if (__SICK.rxq.empty()) {
    __SICK.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    last = __SICK.rxq.back(); // capture last
    __SICK.rxq.clear();
    __SICK.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

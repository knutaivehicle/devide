#include "m8Interface.h"
#include <math.h>
#define PI 3.14159265358979323846

//quanergy
#include <quanergy/client/failover_client.h>              // client; failover adds support for old M8 data
#include <quanergy/parsers/data_packet_parser_00.h>       // parsers for the data packets we want to support
#include <quanergy/parsers/data_packet_parser_01.h>       // - same as above -
#include <quanergy/parsers/data_packet_parser_failover.h> // - same as above -
#include <quanergy/modules/encoder_angle_calibration.h>   // module to apply encoder correction
#include <quanergy/modules/polar_to_cart_converter.h>     // conversion module from polar to Cartesian
//typedefs
/// FailoverClient allows packets to pass through that don't have a header (for old M8 data)
typedef quanergy::client::FailoverClient ClientType;
typedef quanergy::client::VariadicPacketParser<quanergy::PointCloudHVDIRPtr,                     // return type
                                               quanergy::client::DataPacketParserFailover,       // data packet types
                                               quanergy::client::DataPacketParser00,             // - same as above -
                                               quanergy::client::DataPacketParser01> ParserType; // - same as above -
typedef quanergy::client::PacketParserModule<ParserType> ParserModuleType;
typedef quanergy::calibration::EncoderAngleCalibration CalibrationType;
typedef quanergy::client::PolarToCartConverter ConverterType;

//boost log trivial
#include <boost/log/trivial.hpp> // (include only in .cpp)
#define LT_(lvl) BOOST_LOG_TRIVIAL(lvl)

// //
// #include "hrTimestamp.h"

void m8Interface::start()
{
  LT_(info) << "[m8] start thread.";
  b_is_running = true;
  thrd = boost::thread (&m8Interface::threadedFunction, this);
}

void m8Interface::stop()
{
  LT_(info) << "[m8] stop thread.";
  b_is_running = false;
  //**thread join doesn't work nicely. or just takes too long.
  thrd.join();
  LT_(info) << "[m8] thread joined.";
}

// void m8Interface::logStart(std::string strLogFileName_)
// {
//   if (bIsLogging == false) {
//     strLogFileName = strLogFileName_ + s_name + ".csv";
//     bIsStartLogReq = true;
//     LT_(info) << "[" << s_name << "] logging start:" << strLogFileName;
//   }
// }

// void m8Interface::logStop()
// {
//   if (bIsLogging == true) {
//     bIsStartLogReq = false;
//     LT_(info) << "[" << s_name << "] logging stop.";
//   }
// }

uint32_t m8Interface::config_parameters(std::string s_name_, std::string s_ip_, unsigned int u_port_)
{
  s_name = s_name_;
  s_ip = s_ip_;
  s_port = std::to_string(u_port_);
}

void m8Interface::threadedFunction()
{
  // create modules
  ClientType client(s_ip, s_port, 100);
  ParserModuleType parser;
  ConverterType converter;
  CalibrationType calibrator;

  // setup modules
  parser.get<0>().setFrameId("quanergy");
  parser.get<1>().setFrameId("quanergy");
  parser.get<1>().setReturnSelection(quanergy::client::ReturnSelection::MAX);
  parser.get<1>().setDegreesOfSweepPerCloud(360.0);
  parser.get<2>().setFrameId("quanergy");

  // connect modules
  std::vector<boost::signals2::connection> connections;
  connections.push_back(client.connect([&parser](const ClientType::ResultType& pc){ parser.slot(pc); }));

  // //for calibration
  // connections.push_back(parser.connect([&calibrator](const ParserModuleType::ResultType& pc){ calibrator.slot(pc); }));
  // connections.push_back(calibrator.connect([&converter](const CalibrationType::ResultType& pc){ converter.slot(pc); }));

  // // //for manual parameter for calibration
  // // calibrator.setParams(amplitude, phase);

  // no calibration
  connections.push_back(parser.connect([&converter](const ParserModuleType::ResultType& pc){ converter.slot(pc); }));

  //** connect to the callback of this class
  connections.push_back(converter.connect([this](const ConverterType::ResultType& pc){ this->slot(pc); }));

  // run the client with the calibrator and wait for a signal from the
  // calibrator that a successful calibration has been performed
  std::thread client_thread([&client, this] {
    try {
      client.run();
    }
    catch (std::exception& e) {
      LT_(error) << "[m8] Terminating after catching exception: " << e.what() << std::endl;
      this->stop();
    }
  });

  //
  while (b_is_running == true) {

    // //logging - start
    // if (bIsLogging == false && bIsStartLogReq == true) {
    //   bIsStartLogReq = false;
    //   fpLog = fopen(strLogFileName.c_str(), "w");
    //   if (fpLog != NULL) {
    //     bIsLogging = true;
    //   }
    // }

    // //logging - write one by one
    // //==> actual logging will take place in Logic.cpp/h or Logic-main.cpp/h
  
    // //logging - end
    // if (bIsLogging == true && bIsStopLogReq == true) {
    //   bIsStopLogReq = false;
    //   fflush(fpLog);
    //   fclose(fpLog);
    //   bIsLogging = false;
    // }

    //
    boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
  }

  //
  client.stop();
  connections.clear();
  client_thread.join();

  return;
}

void m8Interface::slot(const quanergy::PointCloudXYZIRConstPtr& new_cloud_ptr)
{
  //
  mtx_rxq.lock();

  //save new point cloud.
  m8SCANPoint pt;
  std::vector<m8SCANPoint> pnts;
  quanergy::PointCloudXYZIR new_cloud = (*new_cloud_ptr);
  for (int pp = 0; pp < new_cloud.points.size(); pp++) {
    pt.x = new_cloud.points[pp].x;
    pt.y = new_cloud.points[pp].y;
    
    //orientation
    pt.x = pt.x*cos(rotation_deg * PI / 180) - pt.y*sin(rotation_deg * PI / 180);
    pt.y = pt.x*sin(rotation_deg * PI / 180) + pt.y*cos(rotation_deg * PI / 180);
    //origin
    pt.x = (pt.x + center_x);
    pt.y = (pt.y + center_y);
    
    pt.z = new_cloud.points[pp].z;
    pt.i = new_cloud.points[pp].intensity;
    pt.r = new_cloud.points[pp].ring;
    pnts.push_back(pt);
  }
  rxq.push_back(pnts);

  //get nicer cloud info (one liner)
  std::ostringstream cloudinfo_stream;
  cloudinfo_stream << (*new_cloud_ptr); //pcl cloud has <<(&ostream) operator.
  std::string cloudinfo(cloudinfo_stream.str());
  std::replace(cloudinfo.begin(), cloudinfo.end(), '\r', ',');
  std::replace(cloudinfo.begin(), cloudinfo.end(), '\n', ',');
  // LT_(info) << "[m8] " << cloudinfo;

  //
  mtx_rxq.unlock();
}

m8Interface::m8Interface()
{
  //
  b_is_running = false;

  //defaults
  s_ip = "192.168.1.3";
  s_port = "4141";

  // //
  // bIsLogging = false;
  // bIsStartLogReq = false;
  // bIsStopLogReq = false;
  // strLogFileName = "";

  //oritentation & origin
  center_x = 0;
  center_y = 0;
  rotation_deg = 0;
}

m8Interface::~m8Interface()
{
  // //
  // if (bIsLogging == true)
  // {
  //   logStop();
  // }
  //
  if (b_is_running == true)
  {
    stop();
  }
}

//
// **global instance/interface
//

////**instance (initialization)
m8Interface __M8_FL;
m8Interface __M8_FR;

//
//** RX series return values
//
//   - 0 : no data to get
//   - 0 : yes data to get
//

//RX
int __M8_FL_RX_ALL (__M8_scan_buffer_t & buf)
{
  __M8_FL.mtx_rxq.lock();
  if (__M8_FL.rxq.empty()) {
    __M8_FL.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    buf = __M8_FL.rxq; // capture whole buf
    __M8_FL.rxq.clear();
    __M8_FL.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//RX
int __M8_FL_RX_LAST (__M8_scan_t & last)
{
  __M8_FL.mtx_rxq.lock();
  if (__M8_FL.rxq.empty()) {
    __M8_FL.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    LT_(info) << "[m8(FL)-buffer-level]:" << __M8_FL.rxq.size(); // buffer check
    last = __M8_FL.rxq.back(); // capture last
    __M8_FL.rxq.clear();
    __M8_FL.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//RX
int __M8_FR_RX_ALL (__M8_scan_buffer_t & buf)
{
  __M8_FR.mtx_rxq.lock();
  if (__M8_FR.rxq.empty()) {
    __M8_FR.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    buf = __M8_FR.rxq; // capture whole buf
    __M8_FR.rxq.clear();
    __M8_FR.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//RX
int __M8_FR_RX_LAST (__M8_scan_t & last)
{
  __M8_FR.mtx_rxq.lock();
  if (__M8_FR.rxq.empty()) {
    __M8_FR.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    //MONITORING
    LT_(info) << "[m8(FR)-buffer-level]:" << __M8_FR.rxq.size(); // buffer check
    last = __M8_FR.rxq.back(); // capture last
    __M8_FR.rxq.clear();
    __M8_FR.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

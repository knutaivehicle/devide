//** DO NOT compile this as a separate code!!

//boost log trivial
#include <boost/log/trivial.hpp>
#define LT_(lvl) BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::lvl))
#define __LOG    BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::info))

////**Logic threading service
void Logic::start() {
  LT_(info) << "[logic] start thread.";
  b_is_running = true;
  thrd = boost::thread (&Logic::main, this);
};
void Logic::stop() {
  LT_(info) << "[logic] stop thread.";
  b_is_running = false;
  //
  thrd.join(); 
  LT_(info) << "[logic] thread joined.";
}

////**instance (initialization)
Logic __LOGIC;

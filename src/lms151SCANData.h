#pragma once

////SICK LMS-151

//** refer to the technical doc. : "Telegram Listing"

//** port : 2111

//** protocol : CoLaA

//** sensor firmware version should be > 1.8.0

#include <stdio.h>
#include <stdint.h>
#include <string.h> //strcmp
#include <math.h> //sin, cos

#define PI 3.14159265358979323846

#define SICK_RAW_PNTS_MAX 1082
#define SICK_COV_PNTS_MAX 541

class lms151SCANData
{
  char printbuf[256];
  
 public:
  //xy-orientation & origin
  double center_x;
  double center_y;
  double rotation_deg;

  //
  int dist1     [SICK_RAW_PNTS_MAX];
  int dist_len1;
  //
  int dist2     [SICK_RAW_PNTS_MAX];
  int dist_len2;
  //
  int rssi1     [SICK_RAW_PNTS_MAX];
  int rssi_len1;
  //
  int rssi2     [SICK_RAW_PNTS_MAX];
  int rssi_len2;
  //
  double x      [SICK_COV_PNTS_MAX];
  double y      [SICK_COV_PNTS_MAX];
  double z      [SICK_COV_PNTS_MAX];
  double theta  [SICK_COV_PNTS_MAX];

  lms151SCANData()
  {
    center_x = 0;
    center_y = 0;
    rotation_deg = 0;
  }

  int32_t Print(char * c_str, int32_t length) {

    //clear char buff.
    c_str[0] = '\0';

    for(int i = 0; i < SICK_COV_PNTS_MAX; i++) { // -45deg ~ +225, every 0.5
      sprintf(printbuf,"%f %f %f %f %d\n", theta[i], x[i], y[i], z[i], rssi1[i]);
      strcat(c_str, printbuf);
    }
    
    return 0;
  }
  
  int32_t Log(char * c_str, int32_t length, double dTime)
  {
    int32_t iStatus;
    uint32_t i;

    //
    iStatus = sprintf(c_str, "%.6f\n", dTime);

    for(int i = 0; i < SICK_COV_PNTS_MAX; i++) { // -45deg ~ +225, every 0.5
      sprintf(printbuf,"%f,%f,%f,%f,%d\n", theta[i], x[i], y[i], z[i], rssi1[i]);
      strcat(c_str, printbuf);
    }
    
    return 0;
  }
    
  void parse_scan(char* buff)
  {
    char* tok;
    tok = strtok(buff, " "); //Type of command
    tok = strtok(NULL, " "); //Command
    tok = strtok(NULL, " "); //VersionNumber
    tok = strtok(NULL, " "); //DeviceNumber
    tok = strtok(NULL, " "); //Serial number
    tok = strtok(NULL, " "); //DeviceStatus

    //**
    // devicestatus = atoi(tok);
    // printf("devicestatus=%d\n", devicestatus); //DeviceStatus

    tok = strtok(NULL, " "); //MessageCounter
    tok = strtok(NULL, " "); //ScanCounter
    tok = strtok(NULL, " "); //PowerUpDuration
    tok = strtok(NULL, " "); //TransmissionDuration
    tok = strtok(NULL, " "); //InputStatus
    tok = strtok(NULL, " "); //OutputStatus
    tok = strtok(NULL, " "); //ReservedByteA
    tok = strtok(NULL, " "); //ScanningFrequency
    tok = strtok(NULL, " "); //MeasurementFrequency
    tok = strtok(NULL, " ");
    tok = strtok(NULL, " ");
    tok = strtok(NULL, " ");
    
    tok = strtok(NULL, " "); //NumberEncoders
    int NumberEncoders;
    sscanf(tok, "%d", &NumberEncoders);
    for(int i = 0; i < NumberEncoders; i++)
    {
      tok = strtok(NULL, " "); //EncoderPosition
      tok = strtok(NULL, " "); //EncoderSpeed
    }

    tok = strtok(NULL, " "); //NumberChannels16Bit
    int NumberChannels16Bit;
    sscanf(tok, "%d", &NumberChannels16Bit);
    for(int i = 0; i < NumberChannels16Bit; i++)
    {
      int type = -1; // 0 DIST1 1 DIST2 2 RSSI1 3 RSSI2
      char content[6];
      
      tok = strtok(NULL, " "); //MeasuredDataContent
      sscanf(tok, "%s", content);
      if      (!strcmp(content, "DIST1")) { type = 0; }
      else if (!strcmp(content, "DIST2")) { type = 1; }
      else if (!strcmp(content, "RSSI1")) { type = 2; }
      else if (!strcmp(content, "RSSI2")) { type = 3; }
      
      tok = strtok(NULL, " "); //ScalingFactor
      tok = strtok(NULL, " "); //ScalingOffset
      tok = strtok(NULL, " "); //Starting angle
      tok = strtok(NULL, " "); //Angular step width
      
      tok = strtok(NULL, " "); //NumberData
      int NumberData;
      sscanf(tok, "%X", &NumberData);
      if (type == 0) {
        dist_len1 = NumberData;
      }
      else if (type == 1) {
        dist_len2 = NumberData;
      }
      else if (type == 2) {
        rssi_len1 = NumberData;
      }
      else if (type == 3) {
        rssi_len2 = NumberData;
      }

      // distance (in mm)
      for(int jj = 0; jj < NumberData; jj++)
      {
        int dat;
        tok = strtok(NULL, " "); //data
        sscanf(tok, "%X", &dat);
        if (type == 0) {
          dist1[jj] = dat; //mm (int)
        }
        else if (type == 1) {
          dist2[jj] = dat; //mm (int)
        }
        else if (type == 2) {
          rssi1[jj] = dat; //mm (int)
        }
        else if (type == 3) {
          rssi2[jj] = dat; //mm (int)
        }
      }
    } // end for 16bit

    // calculate x,y,z
    long t, t1, t2;
    float th;
    int kk = 0;
    for(th = -45.0; th <= 225.0; th += 0.5)
    {
      theta[kk] = th;
      
      x[kk] = (double)dist1[kk] * cos(th * PI / 180);
      y[kk] = (double)dist1[kk] * sin(th * PI / 180);

      t     = dist1[kk] * dist1[kk];
      t1    = x[kk] * x[kk] + y[kk] * y[kk];
      t2    = t - t1;
      z[kk] = sqrt(t2);

      //
      x[kk] = x[kk] / 1000.0; //mm --> m
      y[kk] = y[kk] / 1000.0; //mm --> m
      z[kk] = z[kk] / 1000.0; //mm --> m

      //KNUT version START
      // rotation_deg=90;
      // center_x=-3.8;
      // center_y=0;
      //KNUT version END
      
      //orientation
      double temp_x=x[kk] ;
      double temp_y=y[kk] ;
      x[kk] = temp_x*cos(rotation_deg * PI / 180) - temp_y*sin(rotation_deg * PI / 180);
      y[kk] = temp_x*sin(rotation_deg * PI / 180) + temp_y*cos(rotation_deg * PI / 180);
      //origin
      x[kk] = (x[kk] + center_x);
      y[kk] = (y[kk] + center_y);

      kk++;
    }

    int type = -1;
    char content[6];
    tok = strtok(NULL, " "); //MeasuredDataContent
    sscanf(tok, "%s", content);
    if (!strcmp(content, "DIST1")) { type = 0; }
    else if (!strcmp(content, "DIST2")) { type = 1; }
    else if (!strcmp(content, "RSSI1")) { type = 2; }
    else if (!strcmp(content, "RSSI2")) { type = 3; }
    
    tok = strtok(NULL, " "); //ScalingFactor
    tok = strtok(NULL, " "); //ScalingOffset
    tok = strtok(NULL, " "); //Starting angle
    tok = strtok(NULL, " "); //Angular step width
    tok = strtok(NULL, " "); //NumberData
    int NumberData;
    sscanf(tok, "%X", &NumberData);
    if (type == 0) {
      dist_len1 = NumberData;
    }
    else if (type == 1) {
      dist_len2 = NumberData;
    }
    else if (type == 2) {
      rssi_len1 = NumberData;
    }
    else if (type == 3) {
      rssi_len2 = NumberData;
    }
    for (int i = 0; i < NumberData; i++)
    {
      int dat;
      tok = strtok(NULL, " "); //data
      sscanf(tok, "%X", &dat);

      if (type == 0) {
        dist1[i] = dat;
      }
      else if (type == 1) {
        dist2[i] = dat;
      }
      else if (type == 2) {
        rssi1[i] = dat;   // intensity
      }
      else if (type == 3) {
        rssi2[i] = dat;
      }
    }
  }

  lms151SCANData & operator =(const lms151SCANData & source)
  {
    //
    dist_len1 = source.dist_len1;
    dist_len2 = source.dist_len2;
    rssi_len1 = source.rssi_len1;
    rssi_len2 = source.rssi_len2;
    //
    for (int idx = 0; idx < SICK_RAW_PNTS_MAX; idx++)
    {
      dist1 [idx] = source.dist1 [idx];
      dist2 [idx] = source.dist2 [idx];
      rssi1 [idx] = source.rssi1 [idx];
      rssi2 [idx] = source.rssi2 [idx];
    }
    //
    for (int idx = 0; idx < SICK_COV_PNTS_MAX; idx++)
    {
      x     [idx] = source.x     [idx];
      y     [idx] = source.y     [idx];
      z     [idx] = source.z     [idx];
      theta [idx] = source.theta [idx];
    }

    return (*this);
  }
};

#pragma once

//stl
#include <deque>
#include <vector>

//boost
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

//quanergy
#undef Success //--> http://eigen.tuxfamily.org/bz/show_bug.cgi?id=253
#include <quanergy/common/pointcloud_types.h>

//
#include "m8SCANData.h"
#include "hrTimestamp.h"


//
class m8Interface;


//
// **global instance/interface
//

////**types
typedef m8SCANPoint                          __M8_point_t;
typedef std::vector<m8SCANPoint>             __M8_scan_t;
typedef std::deque<std::vector<m8SCANPoint>> __M8_scan_buffer_t;

////**instance (exporting)
extern m8Interface __M8_FL;
extern m8Interface __M8_FR;

////**methods
//RX (1:success, 0:no data)
extern int __M8_FL_RX_ALL     (__M8_scan_buffer_t & buf);
extern int __M8_FL_RX_LAST    (__M8_scan_t & last);
extern int __M8_FR_RX_ALL     (__M8_scan_buffer_t & buf);
extern int __M8_FR_RX_LAST    (__M8_scan_t & last);


class m8Interface {

  //parameters
  std::string s_name;
  std::string s_ip;
  std::string s_port;

  //worker thread obj.
  boost::thread thrd;

  //file logging
  bool bIsLogging;
  bool bIsStartLogReq;
  bool bIsStopLogReq;
  std::string strLogFileName;
  
public:

  //xy-orientation & origin
  double center_x;
  double center_y;
  double rotation_deg;

  //ctor
  m8Interface();
  
  //dtor
  ~m8Interface();
  
  //configuration
  uint32_t config_parameters(std::string s_name_, std::string s_ip_, unsigned int u_port_);
  
  //worker thread function
  void threadedFunction();

  //thread management
  bool b_is_running;
  void start();
  void stop();
  
  //shared
  //std::deque<quanergy::PointCloudXYZIR> rxq;
  std::deque<std::vector<m8SCANPoint>> rxq;

  //mutex
  boost::mutex mtx_rxq;
  
  //callback
  void slot(const quanergy::PointCloudXYZIRConstPtr& new_cloud);

  // //logging
  // void logStart(std::string strLogFileName_);
  // void logStop();
};

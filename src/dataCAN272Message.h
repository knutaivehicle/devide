#pragma once

#include <stdint.h>
#include "i7530aCANMessage.h"

struct dataCAN272Message : public i7530aCANMessage
{
  union {
    struct {
      uint8_t uFlag_56:1;
      uint8_t uFlag_57:1;
      uint8_t uFlag_58:1;
      uint8_t uFlag_59:1;
      uint8_t uFlag_60:1;
      uint8_t uFlag_61:1;
      uint8_t uFlag_62:1;
      uint8_t uFlag_63:1;
    };
    uint8_t uByte7;
  };
  union {
    struct {
      uint8_t uFlag_48:1;
      uint8_t uFlag_49:1;
      uint8_t uFlag_50:1;
      uint8_t uFlag_51:1;
      uint8_t uFlag_52:1;
      uint8_t uFlag_53:1;
      uint8_t uFlag_54:1;
      uint8_t uFlag_55:1;
    };
    uint8_t uByte6;
  };
  union {
    struct {
      uint8_t uFlag_40:1;
      uint8_t uFlag_41:1;
      uint8_t uFlag_42:1;
      uint8_t uFlag_43:1;
      uint8_t uFlag_44:1;
      uint8_t uFlag_45:1;
      uint8_t uFlag_46:1;
      uint8_t uFlag_47:1;
    };
    uint8_t uByte5;
  };
  union {
    struct {
      uint8_t uFlag_32:1;
      uint8_t uFlag_33:1;
      uint8_t uFlag_34:1;
      uint8_t uFlag_35:1;
      uint8_t uFlag_36:1;
      uint8_t uFlag_37:1;
      uint8_t uFlag_38:1;
      uint8_t uFlag_39:1;
    };
    uint8_t uByte4;
  };
  union {
    struct {
      uint8_t uFlag_24:1;
      uint8_t uFlag_25:1;
      uint8_t uFlag_26:1;
      uint8_t uFlag_27:1;
      uint8_t uFlag_28:1;
      uint8_t uFlag_29:1;
      uint8_t uFlag_30:1;
      uint8_t uFlag_31:1;
    };
    uint8_t uByte3;
  };
  union {
    struct {
      uint8_t uFlag_16:1;
      uint8_t uFlag_17:1;
      uint8_t uFlag_18:1;
      uint8_t uFlag_19:1;
      uint8_t uFlag_20:1;
      uint8_t uFlag_21:1;
      uint8_t uFlag_22:1;
      uint8_t uFlag_23:1;
    };
    uint8_t uByte2;
  };
  union {
    struct {
      uint8_t uFlag_8:1;
      uint8_t uFlag_9:1;
      uint8_t uFlag_10:1;
      uint8_t uFlag_11:1;
      uint8_t uFlag_12:1;
      uint8_t uFlag_13:1;
      uint8_t uFlag_14:1;
      uint8_t uFlag_15:1;
    };
    uint8_t uByte1;
  };
  union {
    struct {
      uint8_t uFlag_0:1;
      uint8_t uFlag_1:1;
      uint8_t uFlag_2:1;
      uint8_t uFlag_3:1;
      uint8_t uFlag_4:1;
      uint8_t uFlag_5:1;
      uint8_t uFlag_6:1;
      uint8_t uFlag_7:1;
    };
    uint8_t uByte0;
  };
  
  dataCAN272Message()
  {
    //undefined
    dTimestamp = 0;
    uChannel = 0;
    //defined
    lID = 0x270;
    cMode[0] = 'T';
    cMode[1] = 'x';
    cResvd = 'd';
    uDlc = 8;
    type = 'E';
    //default
    uByte0 = 0;
    uByte1 = 0;
    uByte2 = 0;
    uByte3 = 0;
    uByte4 = 0;
    uByte5 = 0;
    uByte6 = 0;
    uByte7 = 0;
  }

  void packTxCANMsg()
  {
    cData[0] = uByte0;
    cData[1] = uByte1;
    cData[2] = uByte2;
    cData[3] = uByte3;
    cData[4] = uByte4;
    cData[5] = uByte5;
    cData[6] = uByte6;
    cData[7] = uByte7;
  }
};

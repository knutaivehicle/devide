#pragma once

//stl
#include <deque>

//boost
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>

//
#include "controlCenterData.h"

//
#define CONTROL_CENTER_VEHICLE_CHECK  2
#define CONTROL_CENTER_IPCAM_CHECK    5

class controlCenterInterface;


//
// **global instance/interface
//

////**types
typedef controlVehicleData                __CCENTER_VEHICLE_message_t;
typedef controlVehicleUDPData             __CCENTER_VEHICLE_UDP_message_t;
typedef controlIPCAMData                  __CCENTER_IPCAM_message_t;

typedef std::deque<controlVehicleData>    __CCENTER_VEHICLE_message_buffer_t;
typedef std::deque<controlVehicleUDPData> __CCENTER_VEHICLE_UDP_message_buffer_t;
typedef std::deque<controlIPCAMData>      __CCENTER_IPCAM_message_buffer_t;

////**instance (exporting)
extern controlCenterInterface          __CCENTER;

////**methods
//RX (1:success, 0:no data)
extern int __CCENTER_RX_IPCAM_BUF (__CCENTER_IPCAM_message_buffer_t & buf);
extern int __CCENTER_RX_VEHICLE_UDP_BUF (__CCENTER_VEHICLE_UDP_message_buffer_t & buf);
//TX (always 1:success)
extern int __CCENTER_TX_VEHICLE_DATA (__CCENTER_VEHICLE_message_t & msg);



class controlCenterInterface
{
  //parameters
  std::string s_name;
  //
  std::string s_ip_listen;
  unsigned int u_port_listen;
  std::string s_ip_con;
  unsigned int u_port_con;
  std::string s_ip_udp;
  unsigned int u_port_udp; //60000

  //worker thread obj.
  boost::thread thrd_tx;
  boost::thread thrd_rx;
  boost::thread thrd_rxudp;

  //init
  bool bStarted;

  //file logging
  bool bIsLogging_ipcam;    // controlIPCAMData
  bool bIsLogging_control;  // controlVehicleUDPData
  bool bIsStartLogReq_ipcam;
  bool bIsStartLogReq_control;
  bool bIsStopLogReq_ipcam;
  bool bIsStopLogReq_control;
  std::string strLogFileName_ipcam;
  std::string strLogFileName_control;

 public:

  //ctor
  controlCenterInterface();

  //dtor
  ~controlCenterInterface();

  //configuration
  uint32_t config_parameters(std::string s_name_, std::string s_ip_listen_, unsigned int u_port_listen_, std::string s_ip_con_, unsigned int u_port_con_, std::string s_ip_udp_, unsigned int u_port_udp_);

  //thread management TX
  void threadedFunction_tx();
  bool b_is_running_tx;
  std::deque<controlVehicleData> txq; //a transmit buffer for Vehicle data.
  boost::mutex mtx_txq;

  //thread management RX
  void threadedFunction_rx();
  bool b_is_running_rx;
  std::deque<controlIPCAMData> rxq; //a receive buffer for IPCAM data.
  boost::mutex mtx_rxq;

  //thread management RX(UDP)
  void threadedFunction_rxudp();
  bool b_is_running_rxudp;
  std::deque<controlVehicleUDPData> rxqudp; //a receive buffer for IPCAM data.
  boost::mutex mtx_rxqudp;

  //thread start/stop
  void start();
  void stop();

  //log printer
  // controlIPCAMData
  int32_t Log_ipcam(char * c_str, int32_t length, double dTime, controlIPCAMData *data)
  {
    int32_t iStatus;
    uint32_t i;

    iStatus = sprintf(c_str, "%.6f,%lld,%d,%d,%d,%d,%d,%d,%d,%d", \
                      dTime, \
                      data->iObjectID, \
                      data->uDateTime, \
                      data->iLength, \
                      data->iWidth, \
                      data->iLatitude, \
                      data->iLongitude, \
                      data->iLatitude_Vel, \
                      data->iLongitude_Vel, \
                      data->uHeading);
    strcat(c_str, "\n");
    
    return iStatus;
  }

  // controlVehicleUDPData
  int32_t Log_control(char * c_str, int32_t length, double dTime, controlVehicleUDPData *data)
  {
    int32_t iStatus;
    uint32_t i;

    iStatus = sprintf(c_str, "%.6f,%d,%d", \
                      dTime, \
                      data->uAutonomous_Start, \
                      data->uWaypoint);
    strcat(c_str, "\n");

    return iStatus;
  }

  //logging
  void logStart_ipcam  (std::string strLogFileName_);
  void logStart_control(std::string strLogFileName_);
  void logStop_ipcam   ();
  void logStop_control ();
};

extern controlCenterInterface __CCENTER;

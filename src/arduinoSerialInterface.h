#pragma once

//c
#include <stdint.h>
#include <termios.h> // termios

//stl
#include <string>
#include <deque>

//boost
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

//
#include "arduinoSerialMessage.h"

//
class arduinoSerialInterface;


//parameters
#define DIO_RX_BUFF_SIZE_MAX 256
#define DIO_TX_BUFF_SIZE_MAX 256

//
// **global instance/interface
//

////**types
typedef arduinoSerialMessage             __DIO_message_t;
typedef std::deque<arduinoSerialMessage> __DIO_message_buffer_t;

////**instance (exporting)
extern arduinoSerialInterface            __DIO;

////**methods
//RX (1:success, 0:no data)
extern int __DIO_RX_ALL     (__DIO_message_buffer_t & buf);
extern int __DIO_RX_LAST    (__DIO_message_t & last);
//TX (always 1:success)
extern int __DIO_TX_LEDCTRL (__DIO_message_t msg);
extern int __DIO_TX_SWQUERY (void);


class arduinoSerialInterface
{
  //parameters
  std::string s_name;
  std::string s_dev;
  uint32_t u_baud;

  //worker thread obj.
  boost::thread thrd;

  //init
  bool bInited;

  //file desc.
  int fd;

  //saved term setting
  struct termios oldoptions;
  
  //a temp msg buf. + arduino protocol parser/packer
  arduinoSerialMessage tMsg;

  //file logging
  bool bIsLogging;
  bool bIsStartLogReq;
  bool bIsStopLogReq;
  std::string strLogFileName;
  
 public:
  
  //ctor
  arduinoSerialInterface();

  //dtor
  ~arduinoSerialInterface();
  
  //configuration
  uint32_t config_parameters(std::string s_name_, std::string s_dev_, uint32_t u_baud_);

  //worker thread function
  void threadedFunction();

  //thread management
  bool b_is_running;
  void start();
  void stop();
  
  //shared Rx/Tx msg queue
  std::deque<arduinoSerialMessage> rxq;
  std::deque<std::string> txq;
  
  //mutex
  boost::mutex mtx_rxq;
  boost::mutex mtx_txq;

  //logging
  void logStart(std::string strLogFileName_);
  void logStop();
};

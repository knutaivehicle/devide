#pragma once

//stl
#include <vector>
#include <deque>

//of
#include "ofMain.h"
#include "ofAppNoWindow.h"
#define OF_APP_GRAPHICS

//services
#include "i7530aCANInterface.h"     // i-7530a RS232-to-CAN interface
#include "lms151SICKInterface.h"    // lms151 SICK Lidar interface
#include "arduinoSerialInterface.h" // arduino Serial interface (SW/Lamp I/O)
#include "ubloxGPSInterface.h"      // u-blox GPS interface
#include "m8Interface.h"            // m8 Quanergy Lidar interface
#include "controlCenterInterface.h" // network control center interface (Dev. network system)

class Main : public ofBaseApp {

 public:

  ofEasyCam cam;
    
  void setup();
  void update();
  void draw();
  void exit();

  void keyPressed(int key);
  // void keyReleased(int key);
  // void mouseMoved(int x, int y );
  // void mouseDragged(int x, int y, int button);
  // void mousePressed(int x, int y, int button);
  // void mouseReleased(int x, int y, int button);
  // void mouseEntered(int x, int y);
  // void mouseExited(int x, int y);
  // void windowResized(int w, int h);
  // void dragEvent(ofDragInfo dragInfo);
  // void gotMessage(ofMessage msg);

  void startLogging();
  void stopLogging();
};

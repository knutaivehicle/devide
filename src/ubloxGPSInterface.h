#pragma once

//c
#include <stdint.h>
#include <termios.h> // termios

//stl
#include <string>
#include <deque>

//boost
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

//
#include "ubloxGPSMessage.h"

//
class ubloxGPSInterface;


//parameters
#define UBLOX_RX_BUFF_SIZE_MAX 256
#define UBLOX_TX_BUFF_SIZE_MAX 256

//
// **global instance/interface
//

////**types
typedef ubloxGPSMessage                  __GPS_message_t;
typedef std::deque<ubloxGPSMessage>      __GPS_message_buffer_t;

////**instance (exporting)
extern ubloxGPSInterface                 __GPS;

////**methods
//RX (1:success, 0:no data)
extern int __GPS_RX_ALL     (__GPS_message_buffer_t & buf);
extern int __GPS_RX_LAST    (__GPS_message_t & last);



class ubloxGPSInterface
{
  //parameters
  std::string s_name;
  std::string s_dev;
  uint32_t u_baud;

  //worker thread obj.
  boost::thread thrd;
  
  //a temp msg buf. + GPS frame parser
  ubloxGPSMessage tMsg;

  //init
  bool bInited;

  //file desc.
  int fd;

  //saved term setting
  struct termios oldoptions;

  //file logging
  bool bIsLogging;
  bool bIsStartLogReq;
  bool bIsStopLogReq;
  std::string strLogFileName;
  
 public:
  
  //ctor
  ubloxGPSInterface();

  //dtor
  ~ubloxGPSInterface();
  
  //configuration
  uint32_t config_parameters(std::string s_name_, std::string s_dev_, uint32_t u_baud_);

  //worker thread function
  void threadedFunction();

  //thread management
  bool b_is_running;
  void start();
  void stop();
  
  //shared Rx/Tx msg queue
  std::deque<ubloxGPSMessage> rxq;

  //mutex
  boost::mutex mtx_rxq;

  //logging
  void logStart(std::string strLogFileName_);
  void logStop();
};

extern ubloxGPSInterface __GPS;

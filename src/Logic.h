#pragma once

// logic() must be executed as a separate thread
// divided loop time could be achieved by modulus(%) expr. (i.e. x2, x5 etc..)

//
// any resultant or intermediate data should be in the global context!
//
// 1) because ..
//
// drawing thread is different and dedicated thread.
// it is openFrameworks' requirement.
// so, draw() will be called by Main thread
//
// 2) more safe in software safety?
//

// some repetitive drawing methods might be provided as a function for user convenience

//boost
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

//
#define __MSLEEP(msec) (boost::this_thread::sleep_for(boost::chrono::milliseconds(msec)))
#define __USLEEP(usec) (boost::this_thread::sleep_for(boost::chrono::microseconds(usec)))

//boost log trivial
#include <boost/log/trivial.hpp>
#define LT_(lvl) BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::lvl))
#define __LOG    BOOST_LOG_STREAM_WITH_PARAMS(::boost::log::trivial::logger::get(), \
                                              (::boost::log::keywords::severity = ::boost::log::trivial::info))

//profiling..
#include "hrTimestamp.h"

//
class Logic {

  //** (0) init -- << USER CODE >> : one time init.
  void init();

  //** (1) loop -- << USER CODE >> : indep. threaded function: 20ms
  void loop();
  
  //thread control
  boost::thread thrd;
  bool b_is_running;

  //
  hrTimestamp hrtimer;
  
  //thread function
  void main()
  {
    init();

    while(b_is_running == true)
    {
      loop();
      //profiling example
      hrtimer.get_elpsd();
      //__LOG << "[logic/mainloop] hrtimer.d_time:" << hrtimer.d_time; //(sec)
      hrtimer.set();
      boost::this_thread::sleep_for(boost::chrono::milliseconds(u_sleep_time));
    }
  }

public:

  //log file pointer
  FILE * fpLogic;

  //** (2) setup -- << USER CODE >> : called once by drawing engine, for setup.
  void setup();
  
  //** (3) draw -- << USER CODE >> : called by drawing engine: 50fps (20ms) or slower.
  void draw();

  //** (4) overlay -- << USER CODE >> : called by drawing engine: 50fps (20ms) or slower.
  void overlay();

  //** (5) exit -- << USER CODE >> : exit
  void exit();

  //thread controls
  void start();
  void stop();

  //
  uint32_t u_sleep_time;

  //ctor
  Logic() {
    b_is_running = false;
    u_sleep_time = 20; //msec : sleep 20 ms
  }
  
  //dtor
  ~Logic() {
    if (b_is_running == true)
    { stop(); }
  }
};

//
// **global instance/interface
//

////**instance (exporting)
extern Logic __LOGIC;

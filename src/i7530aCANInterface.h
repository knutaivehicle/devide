#pragma once

//c
#include <stdint.h>
#include <termios.h> // termios

//stl
#include <string>
#include <deque>

//boost
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

//
#include "i7530aCANMessage.h"

class i7530aCANInterface;


//parameters
#define I7530A_RX_BUFF_SIZE_MAX 256
#define I7530A_TX_BUFF_SIZE_MAX 256

//
// **global instance/interface
//

////**types
typedef i7530aCANMessage             __CAN_message_t;
typedef std::deque<i7530aCANMessage> __CAN_message_buffer_t;

////**instance (exporting)
extern i7530aCANInterface            __CANCH1;
extern i7530aCANInterface            __CANCH2;

////**methods
//RX (1:success, 0:no data)
extern int __CANCH1_RX_BUF     (__CAN_message_buffer_t & buf); //STD & EXT recv
extern int __CANCH2_RX_BUF     (__CAN_message_buffer_t & buf); //STD & EXT recv
//TX (always 1:success)
extern int __CANCH1_TX     (__CAN_message_t & msg); //EXT send
extern int __CANCH2_TX     (__CAN_message_t & msg); //STD send


class i7530aCANInterface
{
  //parameters
  std::string s_name;
  std::string s_dev;
  uint32_t u_baud;

  //worker thread obj.
  boost::thread thrd;
  
  //a temp msg buf. + rs232-to-CAN frame parser
  i7530aCANMessage tMsg;

  //init
  bool bInited;

  //file desc.
  int fd;

  //saved term setting
  struct termios oldoptions;

  //file logging
  bool bIsLogging;
  bool bIsStartLogReq;
  bool bIsStopLogReq;
  std::string strLogFileName;
  
 public:
  
  //ctor
  i7530aCANInterface();

  //dtor
  ~i7530aCANInterface();
  
  //configuration
  uint32_t config_parameters(std::string s_name_, std::string s_dev_, uint32_t u_baud_);

  //worker thread function
  void threadedFunction();

  //thread management
  bool b_is_running;
  void start();
  void stop();
  
  //shared Rx/Tx msg queue
  std::deque<i7530aCANMessage> rxq;
  std::deque<i7530aCANMessage> txq;

  //mutex
  boost::mutex mtx_rxq;
  boost::mutex mtx_txq;

  //logging
  void logStart(std::string strLogFileName_);
  void logStop();
};

extern i7530aCANInterface __CANCH1;
extern i7530aCANInterface __CANCH2;

#include "Logic.h"
#include "Main.h"
#include "impl/logic.cpp" //internal implementation
//
#include "Logic-main.h"
//#include "Logic-examples.h"

//
#include "hrTimestamp.h"

//// - - - - - - - - - - - - - - - - - - - - - - - - - - -
/// ****global varialbes listing...
//

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ** thread objects
//
// extern arduinoSerialInterface   __DIO;
// extern i7530aCANInterface       __CANCH1;
// extern i7530aCANInterface       __CANCH2;
// extern lms151SICKInterface      __SICK;
// extern m8Interface              __M8_FL;
// extern m8Interface              __M8_FR;
// extern ubloxGPSInterface        __GPS;
// extern Logic                    __LOGIC;
//

//
// **global instance/interface
//

hrTimestamp hrtimer;

//ALGORITHM

void Logic::init()
{
  //logging file for Logic algorithm
  fpLogic = fopen("logfile-Logic.csv", "w");
}

void Logic::exit()
{
  fflush(fpLogic);
  fclose(fpLogic);
}

void Logic::loop() // 10ms
{
  static uint32_t loopcnt = 0;

  //
  loopcnt++;
  
  //**Monitoring variables in terminal log message.
  if (loopcnt % 50 == 0) // every 1 sec
  { __LOG << "[logic] running timer in seconds (loopcnt): " << std::dec << loopcnt/(1000/u_sleep_time) << " sec."; }




  

  Signal_Processing(loopcnt);

  Lidar_Object_Tracking(loopcnt);

  Path_Generation(loopcnt);

  Infra_Information_Processing(loopcnt);

  Collision_Estimation(loopcnt);

  Stop_Check(loopcnt);

  Autonomous_Start_Check(loopcnt);

  Autonomous_Control_Action(loopcnt);

  Output_Signal_Processing(loopcnt);


  // //usage example #1 - DIO
  // DIO_example(loopcnt);

  // //usage example #2 - CAN
  // CAN_example(loopcnt);

  //usage example #3 - GPS
  GPS_example(loopcnt);

  //usage example #6 - Control center
  CONTROL_example(loopcnt);

  //usage example #4 - SICK
  SICK_view(loopcnt);
  
  //usage example #5 - M8
  M8_FL_view(loopcnt);
  
  //usage example #5 - M8
  M8_FR_view(loopcnt);
}

//GRAPHICS

void Logic::setup()
{
  graphics_setup();
}

void Logic::draw()
{
  graphics_draw();
}

void Logic::overlay()
{
  graphics_overlay();
}

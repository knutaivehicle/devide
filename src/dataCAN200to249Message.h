#pragma once

#include <stdint.h>
#include "i7530aCANMessage.h"

struct dataCAN200to249Message : public i7530aCANMessage
{
  union {
    struct {
      uint8_t uByte6;
      uint8_t uByte7;
    };
    uint16_t uInt16_3;
  };
  union {
    struct {
      uint8_t uByte4;
      uint8_t uByte5;
    };
    uint16_t uInt16_2;
  };
  union {
    struct {
      uint8_t uByte2;
      uint8_t uByte3;
    };
    uint16_t uInt16_1;
  };
  union {
    struct {
      uint8_t uByte0;
      uint8_t uByte1;
    };
    uint16_t uInt16_0;
  };
  
  dataCAN200to249Message()
  {
    //undefined
    dTimestamp = 0;
    uChannel = 0;
    //defined
    lID = 0x200; // use one of 0x200 ~ 0x249
    cMode[0] = 'T';
    cMode[1] = 'x';
    cResvd = 'd';
    uDlc = 8;
    type = 'E';
    //default
    uByte0 = 0;
    uByte1 = 0;
    uByte2 = 0;
    uByte3 = 0;
    uByte4 = 0;
    uByte5 = 0;
    uByte6 = 0;
    uByte7 = 0;
  }

  void packTxCANMsg()
  {
    cData[0] = uByte0;
    cData[1] = uByte1;
    cData[2] = uByte2;
    cData[3] = uByte3;
    cData[4] = uByte4;
    cData[5] = uByte5;
    cData[6] = uByte6;
    cData[7] = uByte7;
  }
};

#pragma once

////
// u-blox evk-6T data structure + parser
////

//NOTE
//** baud rate is 115200
//

//NOTE
//** protocol is NMEA-0183
//
//** NMEA-0183 standard protocol
//
// http://www.tronico.fi/OH6NT/docs/NMEA0183.pdf
// p.9
//
// GGA Global Positioning System Fix Data. Time, Position and fix related data
// for a GPS receiver
//                                                      11
//        1         2       3 4        5 6 7  8   9  10 |  12 13  14   15
//        |         |       | |        | | |  |   |   | |   | |   |    |
// $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
//
//  1) Time (UTC)
//  2) Latitude
//  3) N or S (North or South)
//  4) Longitude
//  5) E or W (East or West)
//  6) GPS Quality Indicator,
//  0 - fix not available,
//  1 - GPS fix,
//  2 - Differential GPS fix
//  7) Number of satellites in view, 00 - 12
//  8) Horizontal Dilution of precision
//  9) Antenna Altitude above/below mean-sea-level (geoid)
// 10) Units of antenna altitude, meters
// 11) Geoidal separation, the difference between the WGS-84 earth
//  ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level below ellipsoid
// 12) Units of geoidal separation, meters
// 13) Age of differential GPS data, time in seconds since last SC104
//  type 1 or 9 update, null field when DGPS is not used
// 14) Differential reference station ID, 0000-1023
// 15) Checksum
//
//
// ** u-blox evk-6T TX sample
//
// $GPGGA,215555.00,3658.19044,N,12752.15095,E,1,06,2.96,105.9,M,20.7,M,,*57
//
// $GPGGA,
// 215555.00,    1) Time (UTC)
// 3658.19044,   2) Latitude
// N,            3) N or S (North or South)
// 12752.15095,  4) Longitude
// E,            5) E or W (East or West)
// 1,            6) GPS Quality Indicator,
// 06,           7) Number of satellites in view, 00 - 12
// 2.96,         8) Horizontal Dilution of precision
// 105.9,        9) Antenna Altitude above/below mean-sea-level (geoid)
// M,           10) Units of antenna altitude, meters
// 20.7,        11) Geoidal separation, the difference between the WGS-84 earth
// M,           12) Units of geoidal separation, meters
// (empty),     13) Age of differential GPS data, time in seconds since last SC104
// (empty),     14) Differential reference station ID, 0000-1023 X
// *                *
// 57           15) Checksum
//

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h> //toupper

#define GPS_PARSE_TOKEN_SIZE_MAX 64 // ~50 characters

//boost log trivial
#include <boost/log/trivial.hpp>
#define LT_(lvl) BOOST_LOG_TRIVIAL(lvl)

class ubloxGPSMessage
{
  ////rs232 parsing
  //parsing states
  static const uint8_t IDLE         = 0x00; // wait for '$'
  static const uint8_t GOT_STA_FLAG = 0x01; // collect tokens
  static const uint8_t GOT_END_FLAG = 0x02; // '*', get 2 more and check data integrity. OK or BAD. \
                                                 // if OK, convert. else ERR_COMM
  //somewhat 'minimal' implementation of hex-to-dec without stdio.h
  //-> https://stackoverflow.com/a/27719325
  //* this functions are NOT really confirmed for many situations. use with EXTREME caution.
  uint8_t hexdigit( char hex ) {
    return (hex <= '9') ? hex - '0' : toupper(hex) - 'A' + 10;
  }
  uint64_t hexn8nibbles( const char* hex, unsigned int n ) { //*up to 4 bytes (8 nibbles)
    //
    uint64_t res = 0;
    //
    res |= hexdigit(*(hex));
    for (unsigned int ii = 1; ii < n; ii++) {
      res = res << 4;
      res |= hexdigit(*(hex + ii));
    }
    return res;
  }

public:

  //parsing reports
  static const int8_t OK           = 0x00; // parsing.. need more characters.
  static const int8_t DONE         = 0x01; // **parsed
  //
  static const int8_t ERR_MAX      = 0xFF; // !! too many characters for one token !!
  static const int8_t ERR_COMM     = 0xFE; // !! communication (checksum or size mismatch) error !!
  static const int8_t ERR_UNAVAIL  = 0xFD; // !! data unavailable !!

  double dLat;    // latitude
  double dLongt;  // longitude
  float  fNoise;  // HDOP : Horizontal Dilution of Precision
  float  fHeight; // ant. height (geoid)
  int    iNSat;   // # of satellites

  ubloxGPSMessage() { }

  int8_t parse_char(char c)
  {
    //static local
    static uint8_t status = ubloxGPSMessage::IDLE;
    static std::string tok;
    static std::deque<std::string> tokens;
    static char chksum = 0;
    //
    static std::string content = "";

    content += c;

    //
    switch(status)
    {
    case ubloxGPSMessage::IDLE:
      //a fresh new start
      tok.clear();
      tokens.clear();
      chksum = 0;
      content = "";
      if ('$' == c) { status = ubloxGPSMessage::GOT_STA_FLAG; }
      break;
    case ubloxGPSMessage::GOT_STA_FLAG:
      //limit max. expiration delay for errorneous comm. case
      if (tok.size() > GPS_PARSE_TOKEN_SIZE_MAX) {
        //**too many characters. restart. + error.
        status = ubloxGPSMessage::IDLE;
        return ubloxGPSMessage::ERR_MAX;
      }
      //ended?
      else if ('*' == c) {
        //save the last token
        tokens.push_back(tok);
        tok = "";
        //ok. good. let's see..
        status = ubloxGPSMessage::GOT_END_FLAG;
      }
      //one token end?
      else if (',' == c) {
        tokens.push_back(tok);
        chksum ^= c;
        tok = "";
      }
      //give me more letters...
      else {
        tok += c; //use of : string& operator+= (char c)
        chksum ^= c;
      }
      break;
    case ubloxGPSMessage::GOT_END_FLAG:
      //let me first check integrity.
      tok += c;
      if (tok.size() == 2) {
        //check checksum
        if (chksum == hexn8nibbles(tok.c_str(), 2) && (tokens.size() == 15)) {
          //well done. let's open the bag
          // [ 0] ID : filter in only "GPGGA"
          if ("GPGGA" == tokens[0]) {

            // TOO USE THIS CODE first move this code to .cpp
            // because if include boost/log/trivial.hpp here..
            // preprocessor error..!
            
            // LT_(debug) << "line:" << content;
            // LT_(debug) << "got chksum:" << tok;
            // LT_(debug) << "hav chksum:" << std::hex << (int)chksum;
            // LT_(debug) << "tokens.size:" << std::dec << tokens.size();

            // [ 1] Time (UTC)
            // [ 2] Latitude
            if (tokens[2] == "") { status = ubloxGPSMessage::IDLE; return ubloxGPSMessage::ERR_UNAVAIL; }
            dLat = std::stod(tokens[2]);
            // [ 3] N or S (North or South)
            // [ 4] Longitude
            if (tokens[4] == "") { status = ubloxGPSMessage::IDLE; return ubloxGPSMessage::ERR_UNAVAIL; }
            dLongt = std::stod(tokens[4]);
            // [ 5] E or W (East or West)
            // [ 6] GPS Quality Indicator,
            // [ 7] Number of satellites in view, 00 - 12
            if (tokens[7] == "") { status = ubloxGPSMessage::IDLE; return ubloxGPSMessage::ERR_UNAVAIL; }
            iNSat = std::stoi(tokens[7]);
            // [ 8] Horizontal Dilution of precision
            if (tokens[8] == "") { status = ubloxGPSMessage::IDLE; return ubloxGPSMessage::ERR_UNAVAIL; }
            fNoise = std::stof(tokens[8]);
            // [ 9] Antenna Altitude above/below mean-sea-level (geoid)
            if (tokens[9] == "") { status = ubloxGPSMessage::IDLE; return ubloxGPSMessage::ERR_UNAVAIL; }
            fHeight = std::stof(tokens[9]);
            // [10] Units of antenna altitude, meters
            // [11] Geoidal separation, the difference between the WGS-84 earth
            // [12] Units of geoidal separation, meters
            // [13] Age of differential GPS data, time in seconds since last SC104
            // [14] Differential reference station ID, 0000-1023 X --> not available? there is no such data??
          }

          //
          status = ubloxGPSMessage::IDLE;
          return ubloxGPSMessage::DONE;
        }
        else
        {
          //no good.
          //**comm. disrupted + error.
          status = ubloxGPSMessage::IDLE;
          return ubloxGPSMessage::ERR_COMM;
        }
      }
      break;
    default:
      ;
    } // end of 'ubloxGPSMessage::GOT_STA_FLAG'

    return ubloxGPSMessage::OK; //**frame is building... i need more.
  }

  void Print(char * c_str, int32_t length)
  {
    sprintf(c_str, \
            "(lat:%.6f,longt:%.6f,noise:%.6f,height:%.6f,#ofS:%d)", \
            dLat, \
            dLongt, \
            fNoise, \
            fHeight, \
            iNSat);
  }

  int32_t Log(char * c_str, int32_t length, double dTime)
  {
    int32_t iStatus;
    uint32_t i;

    iStatus = sprintf(c_str, "%.6f,%.6f,%.6f,%.6f,%.6f,%d\n", \
                      dTime, \
                      dLat, \
                      dLongt, \
                      fNoise, \
                      fHeight, \
                      iNSat);

    return iStatus;
  }

  ubloxGPSMessage & operator =(const ubloxGPSMessage & source)
  {
    dLat    = source.dLat;
    dLongt  = source.dLongt;
    fNoise  = source.fNoise;
    fHeight = source.fHeight;
    iNSat   = source.iNSat;

    return (*this);
  }
};

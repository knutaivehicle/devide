#pragma once

#include <stdint.h>
#include "i7530aCANMessage.h"

struct dataCAN192Message
{
  uint8_t uTest;

  void parseRxCANMessage(i7530aCANMessage * ptRxMsg)
  {
    if (ptRxMsg->lID == 0x191)
    {
      uTest = (((int32_t)(ptRxMsg->cData[1]&0x01))>>0);
    }
  }

  int32_t Print(char * c_str, int32_t length){
    int32_t bResult;

    bResult = sprintf(c_str, "uTest:%d\n", uTest);

    return bResult;
  }
};

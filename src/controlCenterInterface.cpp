#include "controlCenterInterface.h"

////system
#include <stdio.h> //sscanf
#include <stdlib.h> //...
#include <string.h> //strlen
#include <sys/select.h> //select, FD_SET, FD_ZERO
#include <sys/time.h> //timeval
#include <sys/types.h>
#include <unistd.h> //read, write
#include <sys/socket.h> //socket
#include <netinet/in.h> //inet_addr
#include <arpa/inet.h> //htons

//boost
//boost log trivial
#include <boost/log/trivial.hpp> // (include only in .cpp)
#define LT_(lvl) BOOST_LOG_TRIVIAL(lvl)

//
#include "hrTimestamp.h"

void controlCenterInterface::start()
{
  LT_(info) << "[ccenter] start thread.";
  b_is_running_tx = true;
  b_is_running_rx = true;
  b_is_running_rxudp = true;
  thrd_tx = boost::thread (&controlCenterInterface::threadedFunction_tx, this);
  thrd_rx = boost::thread (&controlCenterInterface::threadedFunction_rx, this);
  thrd_rxudp = boost::thread (&controlCenterInterface::threadedFunction_rxudp, this);
}

void controlCenterInterface::stop()
{
  LT_(info) << "[ccenter] stop thread.";
  b_is_running_tx = false;
  b_is_running_rx = false;
  b_is_running_rxudp = false;
  //**thread join doesn't work nicely. or just takes too long.
  thrd_tx.join();
  thrd_rx.join();
  thrd_rxudp.join();
  LT_(info) << "[ccenter] thread joined.";
}

void controlCenterInterface::logStart_ipcam(std::string strLogFileName_)
{
  if (bIsLogging_ipcam == false && b_is_running_rx == true) {
    strLogFileName_ipcam = strLogFileName_ + s_name + "-IPCAM" + ".csv";
    bIsStartLogReq_ipcam = true;
    LT_(info) << "[ccenter/ipcam] logging start:" << strLogFileName_ipcam;
  }
}

void controlCenterInterface::logStop_ipcam()
{
  if (bIsLogging_ipcam == true) {
    bIsStartLogReq_ipcam = false;
    LT_(info) << "[ccenter/ipcam] logging stop.";
  }
}

void controlCenterInterface::logStart_control(std::string strLogFileName_)
{
  if (bIsLogging_control == false && b_is_running_rxudp == true) {
    strLogFileName_control = strLogFileName_ + s_name + "-CONTROL" + ".csv";
    bIsStartLogReq_control = true;
    LT_(info) << "[ccenter/control] logging start:" << strLogFileName_control;
  }
}

void controlCenterInterface::logStop_control()
{
  if (bIsLogging_control == true) {
    bIsStartLogReq_control = false;
    LT_(info) << "[ccenter/control] logging stop.";
  }
}

uint32_t controlCenterInterface::config_parameters(std::string s_name_, std::string s_ip_listen_, unsigned int u_port_listen_, std::string s_ip_con_, unsigned int u_port_con_, std::string s_ip_udp_, unsigned int u_port_udp_)
{
  //ip & ports
  s_name = s_name_;
  s_ip_listen = s_ip_listen_;
  u_port_listen = u_port_listen_;
  s_ip_con = s_ip_con_;
  u_port_con = u_port_con_;
  s_ip_udp = s_ip_udp_;
  u_port_udp = u_port_udp_;

  //shared queue
  txq.clear();
  rxq.clear();
  rxqudp.clear();

  return 0;
}

void controlCenterInterface::threadedFunction_tx()
{
  //client
  int len = 0;
  int ret = 0;
  int i = 0;

  //start as a client

  //print out configuration info. to the console.
  LT_(info) << "[ccenter/connect] config: s_ip: " << s_ip_con;
  LT_(info) << "[ccenter/connect] config: u_port: " << u_port_con;

  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = inet_addr(s_ip_con.c_str());
  address.sin_port = htons(u_port_con);
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (!sockfd) {
    LT_(error) << "[ccenter/connect] " << strerror(errno);
    return;
  }
  LT_(info) << "[ccenter/connect] Connecting socket..";
  ret = connect(sockfd, (struct sockaddr *) &address, sizeof(address));
  if (ret) {
    LT_(error) << "[ccenter/connect] " << strerror(errno);
    return;
  }
  //
  int tmp = CONTROL_CENTER_VEHICLE_CHECK; // 2
  write(sockfd, &tmp,sizeof(int)); //send CONTROL_CENTER_VEHICLE_CHECK(==2) to start transmission.

  while (b_is_running_tx == true)
  {
    //TX

    for (i = 0; i < txq.size(); i++) {
      mtx_txq.lock();
      ret = write(sockfd, (void*)&txq[i], sizeof(txq[i])); // check return value!!
      mtx_txq.unlock();
      if (ret == -1) {
        LT_(info) << "[ccenter/connect] Re-connecting socket..";
        ret = connect(sockfd, (struct sockaddr *) &address, sizeof(address));
        if (ret) {
          LT_(error) << "[ccenter/connect] " << strerror(errno);
          return;
        }
        //
        int tmp = CONTROL_CENTER_VEHICLE_CHECK; // 2
        write(sockfd, &tmp,sizeof(int)); //send CONTROL_CENTER_VEHICLE_CHECK(==2) to start transmission.
      }
    }

    //
    // boost::this_thread::sleep_for(boost::chrono::milliseconds(40)); //40ms
    boost::this_thread::sleep_for(boost::chrono::milliseconds(20));
  }

  //close
  close(sockfd);

  return;
}

void controlCenterInterface::threadedFunction_rx()
{
  fd_set reads;
  fd_set cpy_reads;
  int serv_sock;
  int clnt_sock;
  struct sockaddr_in serv_addr;
  struct sockaddr_in clnt_addr;
  socklen_t adr_sz;
  int fd_max;
  int fd_num;
  int i;
  int checkbit[256][1] = {0};
  int check = 0;
  controlIPCAMData cam[7];
  int cam_bit = 0;
  int strlen = 0;

  //start as a server

  //print out configuration info. to the console.
  LT_(info) << "[ccenter/listen] config: s_ip: " << s_ip_listen;
  LT_(info) << "[ccenter/listen] config: u_port: " << u_port_listen;

  //logging
  FILE * fpLog;
  char logbuff[512];
  hrTimestamp tstamp;

  // connect socket
  serv_sock = socket(PF_INET, SOCK_STREAM, 0);
  if (serv_sock < 0)
  {
    LT_(info) << "[ccenter/listen] sock error";
    return;
  }
  memset(&serv_addr, 0x00, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(s_ip_listen.c_str());
  serv_addr.sin_port = htons(u_port_listen);

  if(bind(serv_sock, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) == -1)
  {
    LT_(info) << "[ccenter/listen] bind error";
    return;
  }

  if(listen(serv_sock, 5) == -1)
  {
    LT_(info) << "[ccenter/listen] listen error";
    return;
  }
  FD_ZERO(&reads);
  FD_SET(serv_sock, &reads);
  fd_max = serv_sock;
  
  while (b_is_running_rx == true)
  {
    //logging - start
    if (bIsLogging_ipcam == false && bIsStartLogReq_ipcam == true) {
      bIsStartLogReq_ipcam = false;
      fpLog = fopen(strLogFileName_ipcam.c_str(), "w");
      if (fpLog != NULL) {
        bIsLogging_ipcam = true;
      }
    }

    //RX

    cpy_reads = reads;
    fd_num = select(fd_max + 1, &cpy_reads, 0, 0, 0);
    if(fd_num == -1) {
      LT_(info) << "[ccenter/listen] error: select failed (fd_num == -1)";
      break;
    }
    if(fd_num == 0) {
      // LT_(info) << "[ccenter/listen] waiting for updates.";
      continue;
    }
    //
    cam_bit = 0;
    //
    for(i = 0; i < fd_max + 1; i++)
    {
      if(FD_ISSET(i, &cpy_reads))
      {
        if(i == serv_sock)
        {
          adr_sz = sizeof(clnt_addr);
          clnt_sock = accept(serv_sock, (struct sockaddr*)&clnt_addr, &adr_sz);
          read(clnt_sock, (void*)&check, sizeof(int));
          checkbit[clnt_sock][0] = check; // 여기서 처음연결이 수락되었을때 데이터를 받습니다  모든 카메라는 5라는 체크에 데이터값을 전송합니다
          FD_SET(clnt_sock, &reads);
          if(fd_max < clnt_sock) {
            fd_max = clnt_sock;
          }
          LT_(info) << "[ccenter/listen] connect:" << clnt_sock << " connect module " << checkbit[clnt_sock][0];
          continue;
        }
        else
        {
          switch (checkbit[i][0])
          {
          case CONTROL_CENTER_IPCAM_CHECK: // 5
            strlen = read(i, (void*)&cam[cam_bit], sizeof(cam[cam_bit]));
            if(strlen == 0) {
              FD_CLR(i,&reads);
              close(i);
              LT_(info) << "[ccenter/listen] closed client : " << i;
              //
              cam_bit--;
            }
            else
            {
              //
              mtx_rxq.lock();
              rxq.push_back(cam[cam_bit]);
              mtx_rxq.unlock();

              //logging - write one by one
              if (bIsLogging_ipcam == true) {
                tstamp.get_now();
                Log_ipcam(logbuff, 512, tstamp.d_time, &cam[cam_bit]);
                fprintf(fpLog, logbuff);
                fflush(fpLog);
              }
            }
            //
            cam_bit++;
            break;
          }
        }
      }
    }
    //
    cam_bit = 0;

    //logging - end
    if (bIsLogging_ipcam == true && bIsStopLogReq_ipcam == true) {
      bIsStopLogReq_ipcam = false;
      fflush(fpLog);
      fclose(fpLog);
      bIsLogging_ipcam = false;
    }
        
    //
    // boost::this_thread::sleep_for(boost::chrono::milliseconds(40)); //40ms
    boost::this_thread::sleep_for(boost::chrono::milliseconds(5)); // 5ms
  }

  //close
  close(serv_sock);

  return;
}

void controlCenterInterface::threadedFunction_rxudp()
{
  //
  struct sockaddr_in si_me;
  struct sockaddr_in si_other;
  //
  int s;
  int i;
  socklen_t slen = sizeof(si_other);
  int recv_len;
  char buf[512];
  int res;

  //print out configuration info. to the console.
  LT_(info) << "[ccenter/udp] config: s_ip: " << s_ip_udp;
  LT_(info) << "[ccenter/udp] config: u_port: " << u_port_udp;

  //logging
  FILE * fpLog;
  char logbuff[512];
  hrTimestamp tstamp;

  //create a UDP socket
  s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (s == -1) {
    LT_(info) << "[ccenter/udp] sock error";
    return;
  }

  // zero out the structure
  memset((char *) &si_me, 0, sizeof(si_me));

  si_me.sin_family = AF_INET;
  si_me.sin_port = htons(u_port_udp);
  si_me.sin_addr.s_addr = htonl(INADDR_ANY);

  //bind socket to port
  if(bind(s, (struct sockaddr*)&si_me, sizeof(si_me)) == -1)
  {
    LT_(info) << "[ccenter/udp] bind error";
    return;
  }

  //
  controlVehicleUDPData tmp;

  //keep listening for data
  while(b_is_running_rxudp == true)
  {
    //logging - start
    if (bIsLogging_control == false && bIsStartLogReq_control == true) {
      bIsStartLogReq_control = false;
      fpLog = fopen(strLogFileName_control.c_str(), "w");
      if (fpLog != NULL) {
        bIsLogging_control = true;
      }
    }

    //RX

    //try to receive some data, this is a blocking call
    recv_len = recvfrom(s, &tmp, sizeof(tmp), 0, (struct sockaddr *) &si_other, &slen);
    if (recv_len == -1) {
      LT_(info) << "[ccenter/udp] recvfrom() error";
      return;
    }

    //
    mtx_rxqudp.lock();
    rxqudp.push_back(tmp);
    mtx_rxqudp.unlock();

    //logging - write one by one
    if (bIsLogging_control == true) {
      tstamp.get_now();
      Log_control(logbuff, 512, tstamp.d_time, &tmp);
      fprintf(fpLog, logbuff);
      fflush(fpLog);
    }
    
    //logging - end
    if (bIsLogging_control == true && bIsStopLogReq_control == true) {
      bIsStopLogReq_control = false;
      fflush(fpLog);
      fclose(fpLog);
      bIsLogging_control = false;
    }
        
    // //now reply the client with the same data (echo back)
    // if (sendto(s, buf, recv_len, 0, (struct sockaddr*) &si_other, slen) == -1) {
    //   LT_(info) << "[ccenter/udp] sendto() error";
    //   return;
    // }

    //
    // boost::this_thread::sleep_for(boost::chrono::milliseconds(40)); //40ms
    boost::this_thread::sleep_for(boost::chrono::milliseconds(5)); // 5ms
  }

  //close
  close(s);
  return;
}

//ctor
controlCenterInterface::controlCenterInterface() {

  //
  b_is_running_tx = false;
  b_is_running_rx = false;
  b_is_running_rxudp = false;

  //
  bStarted = false;

  //
  bIsLogging_ipcam = false;
  bIsStartLogReq_ipcam = false;
  bIsStopLogReq_ipcam = false;
  strLogFileName_ipcam = "";
  //
  bIsLogging_control = false;
  bIsStartLogReq_control = false;
  bIsStopLogReq_control = false;
  strLogFileName_control = "";

  //default ip/port preset
  s_ip_listen = std::string("222.116.156.185");
  u_port_listen = 58888;
  s_ip_con = std::string("222.116.156.184");
  u_port_con = 55558;
  s_ip_udp = std::string("222.116.156.185");
  u_port_udp = 60000;
}

//dtor
controlCenterInterface::~controlCenterInterface() {

  //
  if (bIsLogging_ipcam == true)
  {
    logStop_ipcam();
  }
  //
  if (bIsLogging_control == true)
  {
    logStop_control();
  }

  //
  if (b_is_running_tx == true || b_is_running_rx == true || b_is_running_rxudp == true)
  {
    stop();
  }
}

//
// **global instance/interface
//

////**instance (initialization)
controlCenterInterface __CCENTER;

//
//** RX series return values
//
//   - 0 : no data to get
//   - 0 : yes data to get
//

//RX
int __CCENTER_RX_IPCAM_BUF (__CCENTER_IPCAM_message_buffer_t & buf)
{
  __CCENTER.mtx_rxq.lock();
  if (__CCENTER.rxq.empty()) {
    __CCENTER.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    buf = __CCENTER.rxq; // capture whole buf
    __CCENTER.rxq.clear();
    __CCENTER.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//RX
int __CCENTER_RX_VEHICLE_UDP_BUF (__CCENTER_VEHICLE_UDP_message_buffer_t & buf)
{
  __CCENTER.mtx_rxqudp.lock();
  if (__CCENTER.rxqudp.empty()) {
    __CCENTER.mtx_rxqudp.unlock();
    return 0; // no data to get.
  } else {
    buf = __CCENTER.rxqudp; // capture whole buf
    __CCENTER.rxqudp.clear();
    __CCENTER.mtx_rxqudp.unlock();
    return 1; // yes data to get.
  }
}

//TX
int __CCENTER_TX_VEHICLE_DATA (__CCENTER_VEHICLE_message_t & msg)
{
  __CCENTER.mtx_txq.lock();
  __CCENTER.txq.clear(); // no buffering.
  __CCENTER.txq.push_back(msg); // push to send
  __CCENTER.mtx_txq.unlock();

  return 1; // always return with success.
}

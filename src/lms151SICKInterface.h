#pragma once

//stl
#include <deque>

//boost
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

//
#include "lms151SCANData.h"

class lms151SICKInterface;


//
// **global instance/interface
//

////**types
typedef lms151SCANData             __SICK_scan_t;
typedef std::deque<lms151SCANData> __SICK_scan_buffer_t;

////**instance (exporting)
extern lms151SICKInterface         __SICK;

////**methods
//RX (1:success, 0:no data)
extern int __SICK_RX_ALL     (__SICK_scan_buffer_t & buf);
extern int __SICK_RX_LAST    (__SICK_scan_t & last);



class lms151SICKInterface
{
  //parameters
  std::string s_name;
  std::string s_ip;
  unsigned int u_port;

  //worker thread obj.
  boost::thread thrd;
  
  //init
  bool bStarted;

  //file logging
  bool bIsLogging;
  bool bIsStartLogReq;
  bool bIsStopLogReq;
  std::string strLogFileName;
  
 public:
  
  //ctor
  lms151SICKInterface();

  //dtor
  ~lms151SICKInterface();

  //configuration
  uint32_t config_parameters(std::string s_name_, std::string s_ip_, unsigned int u_port_);

  //worker thread function
  void threadedFunction();

  //thread management
  bool b_is_running;
  void start();
  void stop();
  
  //shared Rx msg queue
  std::deque<lms151SCANData> rxq;
  
  //mutex
  boost::mutex mtx_rxq;

  //logging
  void logStart(std::string strLogFileName_);
  void logStop();
};

extern lms151SICKInterface __SICK;

#include <string.h>

//m8 logging
extern void M8_FL_logstart(std::string logpath);
extern void M8_FL_logstop();
extern void M8_FR_logstart(std::string logpath);
extern void M8_FR_logstop();

//views
extern void SICK_view(uint32_t loopcnt);
extern void M8_FL_view(uint32_t loopcnt);
extern void M8_FR_view(uint32_t loopcnt);
extern void graphics_setup();
extern void graphics_draw();
extern void graphics_overlay();
extern void CONTROL_example(uint32_t loopcnt);
extern void GPS_example(uint32_t loopcnt);

//logics
extern void DIO_CAN_Comm(uint32_t loopcnt);
extern void Signal_Processing(uint32_t loopcnt);
extern void Autonomous_Start_Check(uint32_t loopcnt);
extern void Autonomous_Control_Action(uint32_t loopcnt);

extern void Signal_Processing(uint32_t loopcnt);
extern void Lidar_Object_Tracking(uint32_t loopcnt);
extern void Path_Generation(uint32_t loopcnt);
extern void Infra_Information_Processing(uint32_t loopcnt);
extern void Collision_Estimation(uint32_t loopcnt);
extern void Stop_Check(uint32_t loopcnt);
extern void Output_Signal_Processing(uint32_t loopcnt);

typedef struct gps_distance_t 
{
     int Dist;
     int Bearing_Angle;
} gps_distance;

extern gps_distance gps_2_point(double P1_lat, double P1_long, double P2_lat, double P2_long);

#pragma once

#include <stdint.h>
#include "i7530aCANMessage.h"

struct dataCAN271Message : public i7530aCANMessage
{
  union {
    struct {
      uint8_t uInt4_14:4;
      uint8_t uInt4_15:4;
    };
    uint8_t uByte7;
  };
  union {
    struct {
      uint8_t uInt4_12:4;
      uint8_t uInt4_13:4;
    };
    uint8_t uByte6;
  };
  union {
    struct {
      uint8_t uInt4_10:4;
      uint8_t uInt4_11:4;
    };
    uint8_t uByte5;
  };
  union {
    struct {
      uint8_t uInt4_8:4;
      uint8_t uInt4_9:4;
    };
    uint8_t uByte4;
  };
  union {
    struct {
      uint8_t uInt4_6:4;
      uint8_t uInt4_7:4;
    };
    uint8_t uByte3;
  };
  union {
    struct {
      uint8_t uInt4_4:4;
      uint8_t uInt4_5:4;
    };
    uint8_t uByte2;
  };
  union {
    struct {
      uint8_t uInt4_2:4;
      uint8_t uInt4_3:4;
    };
    uint8_t uByte1;
  };
  union {
    struct {
      uint8_t uInt4_0:4;
      uint8_t uInt4_1:4;
    };
    uint8_t uByte0;
  };
  
  dataCAN271Message()
  {
    //undefined
    dTimestamp = 0;
    uChannel = 0;
    //defined
    lID = 0x271;
    cMode[0] = 'T';
    cMode[1] = 'x';
    cResvd = 'd';
    uDlc = 8;
    type = 'E';
    //default
    uByte0 = 0;
    uByte1 = 0;
    uByte2 = 0;
    uByte3 = 0;
    uByte4 = 0;
    uByte5 = 0;
    uByte6 = 0;
    uByte7 = 0;
  }

  void packTxCANMsg()
  {
    cData[0] = uByte0;
    cData[1] = uByte1;
    cData[2] = uByte2;
    cData[3] = uByte3;
    cData[4] = uByte4;
    cData[5] = uByte5;
    cData[6] = uByte6;
    cData[7] = uByte7;
  }
};

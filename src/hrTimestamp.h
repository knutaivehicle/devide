#pragma once

#include <time.h>
#include <stdio.h>

#include <string.h>

class hrTimestamp
{
  struct timespec t_start;
  struct timespec t_end;

 public:
  bool b_is_supported;
  double d_time;
  double d_toffset;
  std::string str_time;

  hrTimestamp ()
  {
    //check for CLOCK_MONOTONIC_RAW
    struct timespec spec;
    if (clock_getres( CLOCK_MONOTONIC_RAW, &spec ) != 0 ) { b_is_supported = false; }
    else { b_is_supported = true; }
    //
    d_toffset = 0.0;
  }

  ~hrTimestamp () {}

  void sync_offset ( hrTimestamp & ref_time )
  {
    get_now();
    ref_time.get_now();
    d_toffset = ref_time.d_time - d_time;
  }

  void get_now ()
  {
    if (clock_gettime( CLOCK_MONOTONIC_RAW, &t_end ) != -1 ) {
      d_time = t_end.tv_sec + ((double)t_end.tv_nsec)/1000000000 + d_toffset;
    }
  }

  void set ()
  {
    clock_gettime( CLOCK_MONOTONIC_RAW, &t_start );
  }

  void get_elpsd ()
  {
    clock_gettime( CLOCK_MONOTONIC_RAW, &t_end );
    d_time = (t_end.tv_sec + ((double)t_end.tv_nsec)/1000000000) - (t_start.tv_sec + ((double)t_start.tv_nsec)/1000000000);
  }

  void to_str_msec ()
  {
    str_time = std::to_string ((int)(d_time*1000 + 0.5)) + " (ms)";
  }
      
  void to_str_usec ()
  {
    str_time = std::to_string ((int)(d_time*1000000 + 0.5)) + " (us)";
  }
};

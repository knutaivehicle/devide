#include "ubloxGPSInterface.h"

////system
#include <sys/types.h> // open
#include <sys/stat.h> // open
#include <fcntl.h> // open
#include <string.h> // memset
#include <termios.h> // termios
#include <unistd.h> // read
//kernel
#include <linux/serial.h> // serial_struct
#include <sys/ioctl.h> //ioctl, TIOCGSERIAL, TIOCSSERIAL
//http://man7.org/linux/man-pages/man2/ioctl_list.2.html

////framework
//boost log trivial
#include <boost/log/trivial.hpp> // (include only in .cpp)
#define LT_(lvl) BOOST_LOG_TRIVIAL(lvl)

//
#include "hrTimestamp.h"

void ubloxGPSInterface::start()
{
  LT_(info) << "[" << s_dev << "] start thread.";
  b_is_running = true;
  thrd = boost::thread (&ubloxGPSInterface::threadedFunction, this);
}

void ubloxGPSInterface::stop()
{
  LT_(info) << "[" << s_dev << "] stop thread.";
  b_is_running = false;
  //**thread join doesn't work nicely. or just takes too long.
  thrd.join();
  LT_(info) << "[" << s_dev << "] thread joined.";
}

void ubloxGPSInterface::logStart(std::string strLogFileName_)
{
  if (bIsLogging == false) {
    strLogFileName = strLogFileName_ + s_name + ".csv";
    bIsStartLogReq = true;
    LT_(info) << "[" << s_name << "] logging start:" << strLogFileName;
  }
}

void ubloxGPSInterface::logStop()
{
  if (bIsLogging == true) {
    bIsStartLogReq = false;
    LT_(info) << "[" << s_name << "] logging stop.";
  }
}

//config parameters
uint32_t ubloxGPSInterface::config_parameters(std::string s_name_, std::string s_dev_, uint32_t u_baud_)
{
  //device name
  s_name = s_name_;
  s_dev = s_dev_;
  u_baud = u_baud_;

  //shared queue
  rxq.clear();
  // txq.clear();
  
  return 0;
}

void ubloxGPSInterface::threadedFunction() {

  //print out configuration info. to the console.
  LT_(info) << "[" << s_dev << "] config: s_dev: " << s_dev;
  LT_(info) << "[" << s_dev << "] config: u_baud: " << u_baud;

  //logging
  FILE * fpLog;
  char logbuff[512];
  hrTimestamp tstamp;

  ////prepare controlling terminal
  //adapted from -> https://github.com/openframeworks/openFrameworks/blob/master/libs/openFrameworks/communication/ofSerial.cpp
  bInited = false;

  //open
  LT_(info) << "[" << s_dev << "] opening " << s_dev << " @ " << u_baud << " bps";
  fd = open(s_dev.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
  if(fd == -1){
    LT_(error) << "[" << s_dev << "] unable to open " << s_dev;
    return;
  }

  //setup - baud
  struct termios options;
  tcgetattr(fd, &oldoptions);
  options = oldoptions;
  switch(u_baud){
  case 300:
    cfsetispeed(&options, B300);
    cfsetospeed(&options, B300);
    break;
  case 1200:
    cfsetispeed(&options, B1200);
    cfsetospeed(&options, B1200);
    break;
  case 2400:
    cfsetispeed(&options, B2400);
    cfsetospeed(&options, B2400);
    break;
  case 4800:
    cfsetispeed(&options, B4800);
    cfsetospeed(&options, B4800);
    break;
  case 9600:
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    break;
  // case 14400:
  //   cfsetispeed(&options, B14400);
  //   cfsetospeed(&options, B14400);
  //   break;
  case 19200:
    cfsetispeed(&options, B19200);
    cfsetospeed(&options, B19200);
    break;
  // case 28800:
  //   cfsetispeed(&options, B28800);
  //   cfsetospeed(&options, B28800);
  //   break;
  case 38400:
    cfsetispeed(&options, B38400);
    cfsetospeed(&options, B38400);
    break;
  case 57600:
    cfsetispeed(&options, B57600);
    cfsetospeed(&options, B57600);
    break;
  case 115200:
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);
    break;
  case 230400:
    cfsetispeed(&options, B230400);
    cfsetospeed(&options, B230400);
    break;
  case 12000000: 
    cfsetispeed(&options, 12000000);
    cfsetospeed(&options, 12000000);	
    break;
  default:
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    LT_(error) << "[" << s_dev << "] cannot set " << u_baud << " bps, setting to 9600";
    break;
  }

  //setup - flags
  options.c_cflag |= (CLOCAL | CREAD);
  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;
  options.c_cflag &= ~CRTSCTS;
  options.c_oflag &= (tcflag_t) ~(OPOST);
  options.c_lflag &= ~(ICANON | ECHO | ISIG);
  options.c_iflag &= (tcflag_t) ~(INLCR | IGNCR | ICRNL | IGNBRK);
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &options); //apply settings

  //using ASYNC_LOW_LATENCY -> https://stackoverflow.com/a/4674755
  struct serial_struct kernel_serial_settings;
  if (ioctl(fd, TIOCGSERIAL, &kernel_serial_settings) == 0) {
    kernel_serial_settings.flags |= ASYNC_LOW_LATENCY;
    ioctl(fd, TIOCSSERIAL, &kernel_serial_settings);
  }
  //TIOCGSERIAL
  //Gets the serial line information. A caller can potentially get a lot of serial line information from the tty device all at once in this call. Some programs (such as setserial and dip) call this function to make sure that the baud rate was properly set and to get general information on what type of device the tty driver controls. The caller passes in a pointer to a large struct of type serial_struct, which the tty driver should fill up with the proper values. Here is an example of how this can be implemented:
  //TIOCSSERIAL
  //Sets the serial line information. This is the opposite of TIOCGSERIAL and allows the user to set the serial line status of the tty device all at once. A pointer to a struct serial_struct is passed to this call, full of data that the tty device should now be set to. If the tty driver does not implement this call, most programs still works properly.
  //ASYNC_LOW_LATENCY
  //https://github.com/torvalds/linux/blob/master/include/uapi/linux/tty_flags.h#L69
  //low_latency
  //Minimize the receive latency of the serial device at the cost of greater CPU utilization. (Normally there is an average of 5-10ms latency before characters are handed off to the line discpline to minimize overhead.) This is off by default, but certain real-time applications may find this useful.

  //opening done.
  bInited = true;
  
  //opening done msg.
  LT_(info) << "[" << s_dev << "] opened " << s_dev << " sucessfully @ " << u_baud << " bps";

  //
  char tbuf[UBLOX_TX_BUFF_SIZE_MAX];
  char rbuf[UBLOX_RX_BUFF_SIZE_MAX];
  int rlen = 0;
  
  //main thread loop
  while (b_is_running == true) {

    //logging - start
    if (bIsLogging == false && bIsStartLogReq == true) {
      bIsStartLogReq = false;
      fpLog = fopen(strLogFileName.c_str(), "w");
      if (fpLog != NULL) {
        bIsLogging = true;
      }
    }

    //GPS Rx
    
    //parsing rs232
    rlen = read(fd, rbuf, UBLOX_RX_BUFF_SIZE_MAX);
    if (rlen == -1) {
      ;//some error. ignoring...
    }
    else {
      for (int ii = 0; ii < rlen; ii++)
      {
        if (tMsg.parse_char(rbuf[ii]) == ubloxGPSMessage::DONE) { //frame parsed?
          //frame received!
          mtx_rxq.lock();
          rxq.push_back(tMsg);
          mtx_rxq.unlock();

          //logging - write one by one
          if (bIsLogging == true) {
            tstamp.get_now();
            tMsg.Log(logbuff, 512, tstamp.d_time);
            fprintf(fpLog, logbuff);
            fflush(fpLog);
          }
        }
        else { } // error reporting?
      }
    }

    //logging - end
    if (bIsLogging == true && bIsStopLogReq == true) {
      bIsStopLogReq = false;
      fflush(fpLog);
      fclose(fpLog);
      bIsLogging = false;
    }
    
    //
    boost::this_thread::sleep_for(boost::chrono::milliseconds(50)); //50ms
  }

  //close & destory serial
  tcsetattr(fd, TCSANOW, &oldoptions);
  close(fd);

  //
  bInited = false;

  return;
}

//ctor
ubloxGPSInterface::ubloxGPSInterface() {
  
  //
  b_is_running = false;

  //
  bInited = false;

  //
  bIsLogging = false;
  bIsStartLogReq = false;
  bIsStopLogReq = false;
  strLogFileName = "";
}

//dtor
ubloxGPSInterface::~ubloxGPSInterface() {
  
  //
  if (bIsLogging == true)
  {
    logStop();
  }

  //
  if (b_is_running == true)
  {
    stop();
  }
  
  //
  if (bInited == true)
  {
    tcsetattr(fd, TCSANOW, &oldoptions);
    close(fd);
  }
}

//
// **global instance/interface
//

////**instance (initialization)
ubloxGPSInterface __GPS;

////**methods

//
//** RX series return values
//
//   - 0 : no data to get
//   - 0 : yes data to get
//

//RX
int __GPS_RX_ALL (__GPS_message_buffer_t & buf)
{
  __GPS.mtx_rxq.lock();
  if (__GPS.rxq.empty()) {
    __GPS.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    buf = __GPS.rxq; // capture whole buf
    __GPS.rxq.clear();
    __GPS.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}

//RX
int __GPS_RX_LAST (__GPS_message_t & last)
{
  __GPS.mtx_rxq.lock();
  if (__GPS.rxq.empty()) {
    __GPS.mtx_rxq.unlock();
    return 0; // no data to get.
  } else {
    last = __GPS.rxq.back(); // capture last
    __GPS.rxq.clear();
    __GPS.mtx_rxq.unlock();
    return 1; // yes data to get.
  }
}
